//
//  MMMCellPalette.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/5/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMMCellPalette : NSObject

#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
alpha:1.0]

#define MMMCOLOR_1 UIColorFromRGB(0xc0cbcc)
#define MMMCOLOR_2 UIColorFromRGB(0x81d8d0) //(0xbccacc)
#define MMMCOLOR_3 UIColorFromRGB(0x89989A)
#define MMMCOLOR_4 UIColorFromRGB(0xedf3e4)
#define MMMCOLOR_5 UIColorFromRGB(0xE9DBE1)

@end
