//
//  NSDictionary+NonNull.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/20/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NonNull)

- (id)nonNullObjectForKey:(id)aKey;

@end
