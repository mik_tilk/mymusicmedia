//
//  NSDictionary+NonNull.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/20/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "NSDictionary+NonNull.h"

@implementation NSDictionary (NonNull)

- (id)nonNullObjectForKey:(id)aKey
{
    id object = [self objectForKey:aKey];
    
    if (object == (id)[NSNull null])
    {
        return nil;
    }
    else
    {
        return object;
    }
}

@end
