//
//  MMMCellState.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/17/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#ifndef MMMCellState_h
#define MMMCellState_h

typedef NS_ENUM(NSUInteger, PlayingCellState)
{
    mNone,
    mPrepearingStreaming,
    mPlaying
};

#endif /* MMMCellState_h */
