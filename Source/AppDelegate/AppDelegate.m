//
//  AppDelegate.m
//  NIXProject
//
//  Created by Egor Zubkov on 1/22/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//  r.0.1.0

#import "AppDelegate.h"
#import "Flurry.h"

#import "MMMBasisCreator.h"

#if (defined(CONFIGURATION_Debug))
#import <SDStatusBarManager.h>
#endif

@interface AppDelegate ()

@property MMMBasisCreator *creator;

- (void)setupFlurry;

@end

@implementation AppDelegate

@synthesize window = window_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#if (defined(CONFIGURATION_Debug))
    
#if TARGET_IPHONE_SIMULATOR
    {
        [[SDStatusBarManager sharedInstance] enableOverrides];
    }
#endif
    
#endif
    _creator = [MMMBasisCreator new];
    
    [_creator setupBasicWindow:[self window]];

    [self setupFlurry];
    
    return YES;
}

#pragma mark - Private

- (void)setupFlurry
{
    static NSString *const NixBundleConfigurationKey = @"Configuration";
    static NSString *const NixAppStoreConfiguration = @"Appstore";
    
    NSString *currentConfiguration = [[NSBundle mainBundle] objectForInfoDictionaryKey:NixBundleConfigurationKey];
    
    if (![currentConfiguration isEqualToString:NixAppStoreConfiguration])
    {
        [Flurry setCrashReportingEnabled:YES];
        [Flurry setSecureTransportEnabled:YES];
        [Flurry setShowErrorInLogEnabled:YES];
        [Flurry setDebugLogEnabled:YES];
        
#ifdef FLURRY_APP_KEY
        [Flurry startSession:FLURRY_APP_KEY];
#else
            //  #warning Please setup Flurry key in pch file
#endif
    }
}

@end
