//
//  MMMProperty.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMProperties.h"

#define MIU_DICTIONARY_SET_POINTER_PROPERTY(dictionary, property)\
    if ([self property] != nil)\
    {\
        [dictionary setObject:[self property] forKey:NSStringFromSelector(@selector(property))];\
    }

@interface MMMProperties ()

@end

@implementation MMMProperties

- (instancetype)init
{
    self = [super init];
    
    NSDictionary *temp = @{@"Continue from begining":@(1), @"Only by WiFi":@(0), @"Load images":@(1)};
    
    if (self)
    {
        _propertiesList = temp;
    }
    
    return self;
}

- (void)setProperty:(NSString *)property toValue:(id)value
{
    NSMutableDictionary *mutable = [_propertiesList mutableCopy];
    mutable[property] = value;
    _propertiesList = [mutable copy];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_propertiesList forKey:NSStringFromSelector(@selector(propertiesList))];
}

// generated
- (id)copyWithZone:(NSZone *)theZone
{
    MMMProperties *copy = [[[self class] alloc] init];

    if (copy)
    {
        [copy setPropertiesList:[self propertiesList]];
    }

    return copy;
}

// generated
- (id)initWithCoder:(NSCoder *)theDecoder
{
    self = [super init];

    if (self)
    {
        NSDictionary *dictionaryForDecoding = [theDecoder decodeObjectForKey:NSStringFromClass([self class])];

        [self setPropertiesList:dictionaryForDecoding[NSStringFromSelector(@selector(propertiesList))]];
    }

    return self;
}

// generated
- (BOOL)isEqual:(MMMProperties *)theObject
{
    if (theObject == self)
    {
        return YES;
    }

    if ([self class] != [theObject class])
    {
        return NO;
    }

    if (([self propertiesList] != [theObject propertiesList] && ![[self propertiesList] isEqualToDictionary:[theObject propertiesList]]))
    {
        return NO;
    }

    return YES;
}

// generated
- (NSString *)description
{
    NSMutableString *description = [NSMutableString new];

    [description appendString:[NSString stringWithFormat:@"propertiesList - %@ \n", [self propertiesList]]];

    return description;
}

// generated
- (NSUInteger)hash
{
    return [[self propertiesList] hash];
}

@end
