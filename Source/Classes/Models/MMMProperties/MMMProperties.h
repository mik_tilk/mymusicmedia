//
//  MMMProperty.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMMProperties : NSObject<NSCoding, NSCopying>

@property(nonatomic, copy) NSDictionary *propertiesList;

- (void)setProperty:(NSString *)property toValue:(id)value;

@end
