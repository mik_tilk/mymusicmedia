//
//  MMMTrack.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMMTrack : NSObject<NSCoding>

@property(nonatomic, copy) NSString *trackId;
@property(nonatomic, copy) NSString *localId;
@property(nonatomic, copy) NSString *sourceMedia;
@property(nonatomic, copy) NSString *artist;
@property(nonatomic, copy) NSString *songTitle;
@property(nonatomic, assign) NSInteger duration;
@property(nonatomic, assign) NSInteger likesCount;
@property(nonatomic, strong) UIImage *artworkImage;
@property(nonatomic, copy) NSString *artworkImageURL;
@property(nonatomic, copy) NSString *artworkImageFilePath;
@property(nonatomic, copy) NSString *streamURL;
@property(nonatomic, copy) NSString *songFilePath;
@property(nonatomic, copy) NSString *waveformImageURL;
@property(nonatomic, copy) NSString *waveformImageFilePath;
@property(nonatomic, strong) UIImage *waveformImage;
@property(nonatomic, assign) BOOL downloadable;
@property(nonatomic, assign) NSInteger size;

@end
