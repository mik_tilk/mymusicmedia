//
//  MMMTrack.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMTrack.h"

@implementation MMMTrack

- (instancetype)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    
    if (self != nil)
    {
        [self setTrackId:[decoder decodeObjectForKey:NSStringFromSelector(@selector(trackId))]];
        [self setLocalId:[decoder decodeObjectForKey:NSStringFromSelector(@selector(localId))]];
        [self setSourceMedia:[decoder decodeObjectForKey:NSStringFromSelector(@selector(sourceMedia))]];
        [self setArtist:[decoder decodeObjectForKey:NSStringFromSelector(@selector(artist))]];
        [self setSongTitle:[decoder decodeObjectForKey:NSStringFromSelector(@selector(songTitle))]];
        [self setDuration:[decoder decodeIntegerForKey:NSStringFromSelector(@selector(duration))]];
        [self setArtworkImage:[decoder decodeObjectForKey:NSStringFromSelector(@selector(artworkImage))]];
        [self setArtworkImageURL:[decoder decodeObjectForKey:NSStringFromSelector(@selector(artworkImageURL))]];
        [self setArtworkImageFilePath:[decoder decodeObjectForKey:NSStringFromSelector(@selector(artworkImageFilePath))]];
        [self setSongFilePath:[decoder decodeObjectForKey:NSStringFromSelector(@selector(songFilePath))]];
        [self setStreamURL:[decoder decodeObjectForKey:NSStringFromSelector(@selector(streamURL))]];
        [self setWaveformImage:[decoder decodeObjectForKey:NSStringFromSelector(@selector(waveformImage))]];
        [self setWaveformImageURL:[decoder decodeObjectForKey:NSStringFromSelector(@selector(waveformImageURL))]];
        [self setWaveformImageFilePath:[decoder decodeObjectForKey:NSStringFromSelector(@selector(waveformImageFilePath))]];
        [self setDownloadable:[decoder decodeBoolForKey:NSStringFromSelector(@selector(downloadable))]];
        [self setSize:[decoder decodeIntegerForKey:NSStringFromSelector(@selector(size))]];
        [self setLikesCount:[decoder decodeIntegerForKey:NSStringFromSelector(@selector(likesCount))]];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:_trackId forKey:NSStringFromSelector(@selector(trackId))];
    [encoder encodeObject:_localId forKey:NSStringFromSelector(@selector(localId))];
    [encoder encodeObject:_sourceMedia forKey:NSStringFromSelector(@selector(sourceMedia))];
    [encoder encodeObject:_artist forKey:NSStringFromSelector(@selector(artist))];
    [encoder encodeObject:_songTitle forKey:NSStringFromSelector(@selector(songTitle))];
    [encoder encodeInteger:_duration forKey:NSStringFromSelector(@selector(duration))];
    [encoder encodeObject:_artworkImage forKey:NSStringFromSelector(@selector(artworkImage))];
    [encoder encodeObject:_artworkImageURL forKey:NSStringFromSelector(@selector(artworkImageURL))];
    [encoder encodeObject:_artworkImageFilePath forKey:NSStringFromSelector(@selector(artworkImageFilePath))];
    [encoder encodeObject:_songFilePath forKey:NSStringFromSelector(@selector(songFilePath))];
    [encoder encodeObject:_streamURL forKey:NSStringFromSelector(@selector(streamURL))];
    [encoder encodeObject:_waveformImage forKey:NSStringFromSelector(@selector(waveformImage))];
    [encoder encodeObject:_waveformImageURL forKey:NSStringFromSelector(@selector(waveformImageURL))];
    [encoder encodeObject:_waveformImageFilePath forKey:NSStringFromSelector(@selector(waveformImageFilePath))];
    [encoder encodeBool:_downloadable forKey:NSStringFromSelector(@selector(downloadable))];
    [encoder encodeInteger:_size forKey:NSStringFromSelector(@selector(size))];
    [encoder encodeInteger:_likesCount forKey:NSStringFromSelector(@selector(likesCount))];
}

- (BOOL)isEqual:(id)object
{
    if (self == object)
    {
        return YES;
    }
    
    if ([self class] != [object class])
    {
        return NO;
    }
    
    if ([self hash] == [object hash])
    {
        return YES;
    }
        
    return NO;
}

- (NSUInteger)hash
{
    NSUInteger mediaHash = [[self sourceMedia] hash];
    NSUInteger idHash = [[self trackId] hash];
    
    return mediaHash ^ idHash;
}

@end
