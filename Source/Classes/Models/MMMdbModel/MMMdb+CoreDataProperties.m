//
//  MMMdb+CoreDataProperties.m
//
//
//  Created by Kholomeev on 10/20/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MMMdb+CoreDataProperties.h"

@implementation MMMdb (CoreDataProperties)

@dynamic trackId;
@dynamic localId;
@dynamic sourceMedia;
@dynamic artist;
@dynamic songTitle;
@dynamic duration;
@dynamic likesCount;
@dynamic pictureURL;
@dynamic pictureFile;
@dynamic streamURL;
@dynamic songFilePath;
@dynamic waveformFile;
@dynamic waveformURL;
@dynamic downloadable;
@dynamic size;

@end
