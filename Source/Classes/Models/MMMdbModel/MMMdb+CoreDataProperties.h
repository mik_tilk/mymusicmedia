//
//  MMMdb+CoreDataProperties.h
//
//
//  Created by Kholomeev on 10/20/15.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MMMdb.h"

NS_ASSUME_NONNULL_BEGIN

@interface MMMdb (CoreDataProperties)

@property(nullable, nonatomic, retain) NSString *trackId;
@property(nullable, nonatomic, retain) NSString *localId;
@property(nullable, nonatomic, retain) NSString *sourceMedia;
@property(nullable, nonatomic, retain) NSString *artist;
@property(nullable, nonatomic, retain) NSString *songTitle;
@property(nullable, nonatomic, retain) NSNumber *duration;
@property(nullable, nonatomic, retain) NSNumber *likesCount;
@property(nullable, nonatomic, retain) NSString *pictureURL;
@property(nullable, nonatomic, retain) NSString *pictureFile;
@property(nullable, nonatomic, retain) NSString *songFilePath;
@property(nullable, nonatomic, retain) NSString *streamURL;
@property(nullable, nonatomic, retain) NSString *waveformURL;
@property(nullable, nonatomic, retain) NSString *waveformFile;
@property(nullable, nonatomic, retain) NSNumber *downloadable;
@property(nullable, nonatomic, retain) NSNumber *size;

@end

NS_ASSUME_NONNULL_END
