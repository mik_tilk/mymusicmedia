//
//  MMMdb.m
//
//
//  Created by Kholomeev on 10/20/15.
//
//

#import "MMMdb.h"
#import "MMMTrack.h"

@implementation MMMdb

- (MMMTrack *)model
{
    MMMTrack *track = [MMMTrack new];
    
    if (track)
    {
        [track setTrackId:[self valueForKey:NSStringFromSelector(@selector(trackId))]];
        [track setLocalId:[self valueForKey:NSStringFromSelector(@selector(localId))]];
        [track setSourceMedia:[self valueForKey:NSStringFromSelector(@selector(sourceMedia))]];
        [track setArtist:[self valueForKey:NSStringFromSelector(@selector(artist))]];
        [track setSongTitle:[self valueForKey:NSStringFromSelector(@selector(songTitle))]];
        [track setDuration:[[self valueForKey:NSStringFromSelector(@selector(duration))] integerValue]];
        [track setArtworkImageURL:[self valueForKey:NSStringFromSelector(@selector(artworkImageURL))]];
        [track setArtworkImageFilePath:[self valueForKey:NSStringFromSelector(@selector(artworkImageFilePath))]];
        [track setStreamURL:[self valueForKey:NSStringFromSelector(@selector(streamURL))]];
        [track setSongFilePath:[self valueForKey:NSStringFromSelector(@selector(songFilePath))]];
        [track setWaveformImageURL:[self valueForKey:NSStringFromSelector(@selector(waveformImageURL))]];
        [track setWaveformImageFilePath:[self valueForKey:NSStringFromSelector(@selector(waveformImageFilePath))]];
        [track setDownloadable:[[self valueForKey:NSStringFromSelector(@selector(downloadable))] boolValue]];
        [track setSize:[[self valueForKey:NSStringFromSelector(@selector(size))] integerValue]];
        [track setLikesCount:[[self valueForKey:NSStringFromSelector(@selector(likesCount))] integerValue]];
    }

    return track;
}

- (void)mapTrackToDB:(MMMTrack *)track
{
    [self setValue:[track trackId] forKey:NSStringFromSelector(@selector(trackId))];
    [self setValue:[track localId] forKey:NSStringFromSelector(@selector(localId))];
    [self setValue:[track sourceMedia] forKey:NSStringFromSelector(@selector(sourceMedia))];
    [self setValue:[track artist] forKey:NSStringFromSelector(@selector(artist))];
    [self setValue:[track songTitle] forKey:NSStringFromSelector(@selector(songTitle))];
    [self setValue:@([track duration]) forKey:NSStringFromSelector(@selector(duration))];
    [self setValue:@([track likesCount]) forKey:NSStringFromSelector(@selector(likesCount))];
    [self setValue:[track artworkImageURL] forKey:NSStringFromSelector(@selector(artworkImageURL))];
    [self setValue:[track artworkImageFilePath] forKey:NSStringFromSelector(@selector(artworkImageFilePath))];
    [self setValue:[track streamURL] forKey:NSStringFromSelector(@selector(streamURL))];
    [self setValue:[track songFilePath] forKey:NSStringFromSelector(@selector(songFilePath))];
    [self setValue:[track waveformImageURL] forKey:NSStringFromSelector(@selector(waveformImageURL))];
    [self setValue:[track waveformImageFilePath] forKey:NSStringFromSelector(@selector(waveformImageFilePath))];
    [self setValue:@([track downloadable]) forKey:NSStringFromSelector(@selector(downloadable))];
    [self setValue:@([track size]) forKey:NSStringFromSelector(@selector(size))];
}

@end
