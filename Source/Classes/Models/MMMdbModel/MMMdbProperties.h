//
//  MMMdbProperties.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MMMProperties;

NS_ASSUME_NONNULL_BEGIN

@interface MMMdbProperties : NSManagedObject

- (MMMProperties *)model;
- (void)mapPropertiesToDB:(MMMProperties *)properties;

@end

NS_ASSUME_NONNULL_END

#import "MMMdbProperties+CoreDataProperties.h"
