//
//  MMMdb.h
//
//
//  Created by Kholomeev on 10/20/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@class MMMTrack;

@interface MMMdb : NSManagedObject

- (MMMTrack *)model;
- (void)mapTrackToDB:(MMMTrack *)track;

@end

NS_ASSUME_NONNULL_END

#import "MMMdb+CoreDataProperties.h"
