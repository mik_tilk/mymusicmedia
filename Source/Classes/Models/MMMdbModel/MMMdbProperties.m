//
//  MMMdbProperties.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMdbProperties.h"
#import "MMMProperties.h"

@implementation MMMdbProperties

- (MMMProperties *)model
{
    MMMProperties *properties = [MMMProperties new];
    
    if (properties)
    {
        [properties setPropertiesList:[self valueForKey:NSStringFromSelector(@selector(propertiesList))]];
    }
    
    return properties;
}

- (void)mapPropertiesToDB:(MMMProperties *)properties;
{
    [self setValue:([properties propertiesList]) forKey:NSStringFromSelector(@selector(propertiesList))];
}

@end
