//
//  MMMdbProperties+CoreDataProperties.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MMMdbProperties+CoreDataProperties.h"

@implementation MMMdbProperties (CoreDataProperties)

@dynamic propertiesList;

@end
