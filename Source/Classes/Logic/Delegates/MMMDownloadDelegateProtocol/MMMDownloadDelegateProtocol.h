//
//  MMMDownloadDelegateProtocol.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/23/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMMTrack;

@protocol MMMDownloadDelegateProtocol

- (void)searchResultsListDidChangedTrack:(MMMTrack *)track;
- (void)didDownloadSongData:(NSData *)data forTrack:(MMMTrack *)track;

@end