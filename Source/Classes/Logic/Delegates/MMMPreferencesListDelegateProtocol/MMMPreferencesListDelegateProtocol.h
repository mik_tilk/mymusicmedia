//
//  MMMPreferencesListDelegateProtocol.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/11/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMMProperties;

@protocol MMMPreferencesListDelegateProtocol<NSObject>

- (void)propertiesDidChanged:(MMMProperties *)properties;

@end
