//
//  MMMRequestPreferencesProtocol.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/9/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMMRequestPreferencesProtocol

- (BOOL)mustContinueFromBegining;
- (BOOL)isAllowedToDownload;
- (BOOL)mustLoadImage;

@end
