//
//  MMMNextTrackDelegateProtocol.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/26/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMCellState.h"

@class MMMPlayController;
@class MMMTrack;

@protocol MMMPlayerDelegateProtocol

- (void)player:(MMMPlayController *)player didFinishedPlayTrack:(MMMTrack *)track;
- (void)player:(MMMPlayController *)player didStopPlayTrack:(MMMTrack *)track;
- (void)player:(MMMPlayController *)player didUpdatePlayingProgress:(CGFloat)time forTrack:(MMMTrack *)track;
- (void)player:(MMMPlayController *)player didChagedPlayingState:(PlayingCellState)playingState forTrack:(MMMTrack *)track;

@end
