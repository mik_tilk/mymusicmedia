    //
    //  MMMTrackListControllerDelegateProtocol.h
    //  MyMusicMedia
    //
    //  Created by Kholomeev on 10/6/15.
    //  Copyright © 2015 NIX. All rights reserved.
    //

#import <Foundation/Foundation.h>
#import "MMMCellState.h"

@class MMMTrack;
@class MMMPlayTrackCell;

@protocol MMMTrackListControllerDelegateProtocol

- (void)trackListDidChanged:(NSArray *)trackList;
- (void)updatePlayingProgress:(CGFloat)time forTrack:(MMMTrack *)model;
- (void)updateCellState:(PlayingCellState)cellState forTrack:(MMMTrack *)model;

@optional
- (void)resignFirstResponder;
- (void)trackDidChanged:(MMMTrack *)track;
- (void)updateVisibleCellFromTrack:(MMMTrack *)track byIndex:(NSInteger)index;

@end
