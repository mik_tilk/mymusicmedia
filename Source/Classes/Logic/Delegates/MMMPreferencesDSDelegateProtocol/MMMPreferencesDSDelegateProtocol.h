//
//  MMMPreferencesDSDelegateProtocol.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMMPreferencesDSDelegateProtocol

- (void)didChangeProperty:(NSString *)property;

@end
