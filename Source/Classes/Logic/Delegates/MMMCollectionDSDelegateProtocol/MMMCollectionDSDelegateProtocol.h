//
//  MMMCollectionDSDelegateProtocol.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/29/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMMTrack;
@class MMMPlayTrackCell;

@protocol MMMCollectionDSDelegateProtocol

- (void)willPlayTrack:(MMMTrack *)track;
- (void)seekPlayPosition:(CGFloat)position forTrack:(MMMTrack *)track;
- (void)trackListDidChanged:(NSArray *)trackList;

@optional
- (void)trackDidChanged:(MMMTrack *)track;
- (void)willAddTrack:(MMMTrack *)track;
- (void)willDragCell:(MMMPlayTrackCell *)cell withTrack:(MMMTrack *)track;
- (void)saveTrackList:(NSArray<MMMTrack *> *)trackList;
- (void)deleteTrack:(MMMTrack *)track;

@end
