//
//  MMMDragCellController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMMPlayListCollectionDS;

@interface MMMDragCellController : NSObject<UIGestureRecognizerDelegate>

- (void)setupWithPlayListCollectionDS:(MMMPlayListCollectionDS *)collectionDS;
- (void)handlePanOnPage:(UILongPressGestureRecognizer *)gesture;
@end
