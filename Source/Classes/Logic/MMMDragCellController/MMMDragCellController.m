//
//  MMMDragCellController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMDragCellController.h"
#import "MMMPlayListCollectionDS.h"
#import "MMMPlayTrackCell.h"
#import "MMMPlayPageVC.h"
#import "MMMTrack.h"

@interface MMMDragCellController ()

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(weak, nonatomic) IBOutlet MMMPlayPageVC *playPageVC;

@property(nonatomic, strong) MMMPlayListCollectionDS *playListDS;
@property(nonatomic, assign, getter = isDragging) BOOL dragging;
@property(nonatomic, strong) MMMTrack *draggedModel;
@property(nonatomic, strong) MMMPlayTrackCell *draggedCell;
@property(nonatomic, strong) UIView *draggedCellSnapshot;
@property(nonatomic, assign) CGPoint delta;
@property(nonatomic, strong) UIView *optionsView;
@property(nonatomic, strong) UIImageView *trashBinImageView;
@property(nonatomic, assign, getter= isMagnifiedTrashBinImageView) BOOL magnifyTrashBinImageView;
@end

@implementation MMMDragCellController

- (void)setupWithPlayListCollectionDS:(MMMPlayListCollectionDS *)collectionDS
{
    _playListDS = collectionDS;

    [self initOptionsViewForDraggedCell];
    
    _dragging = NO;
    _draggedCell = nil;
    _draggedCellSnapshot = nil;
    [_draggedCellSnapshot setHidden:!_dragging];
}

- (void)initOptionsViewForDraggedCell
{
    _optionsView = [[UIView alloc] initWithFrame:CGRectMake(.0, +20.0, 100., 50.)];
    [_optionsView setBackgroundColor:MMMCOLOR_4];
    
    CGPoint center = CGPointMake([[_playPageVC view] center].x, -[_optionsView center].y);
    [_optionsView setCenter:center];
    
    _trashBinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"garbage21.png"]];
    [_optionsView addSubview:_trashBinImageView];
    [_trashBinImageView setCenter:CGPointMake([_optionsView frame].size.width / 2, [_optionsView frame].size.height / 2)];
    
    [[_playPageVC view] addSubview:_optionsView];
}

- (void)updateDraggedCellStateByGestureLocation:(CGPoint)point
{
    CGPoint touchPointInCollection = [_collectionView convertPoint:point fromView:[_playPageVC view]];
    CGPoint touchPointInOption = [_optionsView convertPoint:point fromView:[_playPageVC view]];
    
    BOOL isInsideOptionView = CGRectContainsPoint([_optionsView bounds], touchPointInOption);
    BOOL isInsideCollectionView = CGRectContainsPoint([_collectionView bounds], touchPointInCollection);
    
    if (isInsideOptionView || isInsideCollectionView)
    {
        _draggedCellSnapshot.alpha = .9;
    }
    else
    {
        _draggedCellSnapshot.alpha = .2;
    }
}

- (void)updateTrashBinStateByGestureLocation:(CGPoint)point
{
    CGPoint touchPointInOption = [_optionsView convertPoint:point fromView:[_playPageVC view]];
    
    BOOL isInsideTrashBinImageView = CGRectContainsPoint([_trashBinImageView frame], touchPointInOption);
    
    if (isInsideTrashBinImageView)
    {
        if (![self isMagnifiedTrashBinImageView] && [self isDragging])
        {
            [UIView animateWithDuration:.25 animations:^
            {
                _trashBinImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                [_trashBinImageView setImage:[UIImage imageNamed:@"garbage11.png"]];
                _draggedCellSnapshot.alpha = .5;
            }
                completion:^(BOOL finished)
                {
                    _magnifyTrashBinImageView = YES;
                }];
        }
    }
    else
    {
        if ([self isMagnifiedTrashBinImageView])
        {
            [UIView animateWithDuration:.25 animations:^
            {
                _trashBinImageView.transform = CGAffineTransformMakeScale(0.9, 0.9);
                [_trashBinImageView setImage:[UIImage imageNamed:@"garbage21.png"]];
                _draggedCellSnapshot.alpha = .9;
            }
                completion:^(BOOL finished)
                {
                    _magnifyTrashBinImageView = NO;
                }];
        }
    }
}

- (void)handlePanOnPage:(UILongPressGestureRecognizer *)gesture
{
    CGPoint touchPoint = [gesture locationInView:[_playPageVC view]];
    CGPoint touchPointInCollection = [gesture locationInView:_collectionView];
    
    NSIndexPath *indexPath = [_collectionView indexPathForItemAtPoint:touchPointInCollection];
    
    switch ([gesture state])
    {
        case UIGestureRecognizerStateBegan:
        {
            if (indexPath == nil)
            {
                return;
            }
            
            [self setDragging:YES];

            _draggedCell = (MMMPlayTrackCell *)[_collectionView cellForItemAtIndexPath:indexPath];
            
            _delta = CGPointMake([_draggedCell center].x - touchPointInCollection.x, [_draggedCell center].y - touchPointInCollection.y);

            _draggedCellSnapshot = [_draggedCell snapshotViewAfterScreenUpdates:YES];
            [_draggedCellSnapshot setHidden:!_dragging];
            [_draggedCellSnapshot setCenter:SumCGPoints(touchPoint, _delta)];

            [UIView animateWithDuration:.25 animations:^
            {
                _draggedCellSnapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                _draggedCellSnapshot.alpha = .9;
                _draggedCell.alpha = 0.0;
                CGPoint center = CGPointMake([[_playPageVC view] center].x, -[_optionsView center].y);
                [_optionsView setCenter:center];
            }
                completion:^(BOOL finished)
                {
                    [_draggedCell setHidden:_dragging];
                }];
            
            [[_playPageVC view] addSubview:_draggedCellSnapshot];
        }
        break;
            
        case UIGestureRecognizerStateChanged:
        {
            if ([self isDragging])
            {
                [_draggedCellSnapshot setCenter:SumCGPoints(touchPoint, _delta)];
                [self updateDraggedCellStateByGestureLocation:touchPoint];
                [self updateTrashBinStateByGestureLocation:touchPoint];

                [_playListDS dragCellFromIndex:[_collectionView indexPathForCell:_draggedCell] toPoint:touchPointInCollection];
            }
        }
        break;

        default:
        {
            if ([self isDragging])
            {
                MLOG(@"DROP");
                _dragging = NO;
                
                if ([self isMagnifiedTrashBinImageView])
                {
                    [UIView animateWithDuration:.25 animations:^
                    {
                        [_draggedCellSnapshot setCenter:[_trashBinImageView center]];
                        _draggedCellSnapshot.transform = CGAffineTransformMakeScale(.05, .05);
                    }
                        completion:^(BOOL finished)
                        {
                            [_draggedCellSnapshot setHidden:!_dragging];
                            [_draggedCell setHidden:_dragging];
                            
                            [_playListDS deleteTrackByIndex:[_collectionView indexPathForCell:_draggedCell]];
                            
                            [UIView animateWithDuration:.25 animations:^
                            {
                                [self updateTrashBinStateByGestureLocation:touchPoint];
                                
                                CGPoint center = CGPointMake([[_playPageVC view] center].x, -[_optionsView center].y);
                                [_optionsView setCenter:center];
                            }
                                completion:^(BOOL finished)
                                {
                                }
                            ];
                        }
                    ];
                }
                else
                {
                    [UIView animateWithDuration:.25 animations:^
                    {
                        UICollectionViewCell *cell = [_collectionView cellForItemAtIndexPath:indexPath];

                        [_draggedCellSnapshot setCenter:cell ? [cell center] : [_draggedCell center]];
                        [_draggedCell setAlpha:1];
                        
                        CGPoint center = CGPointMake([[_playPageVC view] center].x, -[_optionsView center].y);
                        [_optionsView setCenter:center];
                    }
                        completion:^(BOOL finished)
                        {
                            [_draggedCellSnapshot setHidden:!_dragging];
                            [_draggedCell setHidden:_dragging];
                            _draggedCell = nil;
                        }];
                    
                    [_playListDS saveTrackList];
                }
            }
        }
    }
}

#pragma mark - private

CGPoint SumCGPoints(CGPoint p1, CGPoint p2)
{
    return (CGPoint){p1.x + p2.x, p1.y + p2.y};
}
             
@end
