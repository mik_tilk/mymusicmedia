//
//  MMMTrackListCollectionDS.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMBaseCollectionDS.h"

@interface MMMPlayListCollectionDS : MMMBaseCollectionDS

- (void)dragCellFromIndex:(NSIndexPath *)indexFrom toPoint:(CGPoint)pointTo;
- (void)saveTrackList;
- (void)deleteTrackByIndex:(NSIndexPath *)indexPath;

@end
