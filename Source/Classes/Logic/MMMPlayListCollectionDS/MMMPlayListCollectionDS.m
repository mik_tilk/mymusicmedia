//
//  MMMTrackListCollectionDS.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionDS_Protected.h"
#import "MMMPlayListCollectionDS.h"
#import "MMMPlayTrackCell.h"

@implementation MMMPlayListCollectionDS

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        identifier = NSStringFromClass([MMMPlayTrackCell class]);
    });
    
    MMMPlayTrackCell *cell = (MMMPlayTrackCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                           forIndexPath:indexPath];

    MMMDisplayingCellModel *viewModel = [[self viewModelsList] objectAtIndex:[indexPath item]];
    [cell setupWithDisplayingModel:viewModel];
    
    [cell setDidCellTapHandler:^(MMMBaseCell *cell)
    {
        [[self delegate] willPlayTrack:[self modelByViewModel:viewModel]];
    }];
    
    [cell setDidWaveformPanHandler:^(CGFloat position, MMMBaseCell *cell)
    {
        [[self delegate] seekPlayPosition:position forTrack:[self modelByViewModel:viewModel]];
    }];
    
    return cell;
}

- (void)dragCellFromIndex:(NSIndexPath *)indexFrom toPoint:(CGPoint)pointTo
{
    UICollectionView *collectionView = [self collectionView];
    NSIndexPath *indexTo = [collectionView indexPathForItemAtPoint:pointTo];
    MMMPlayTrackCell *cellTo = (MMMPlayTrackCell *)[collectionView cellForItemAtIndexPath:indexTo];

    if (pointTo.y > ([collectionView frame].size.height - [cellTo bounds].size.height / 2))
    {
        NSIndexPath *indexPathToScroll = [self getMaxVisibleIndexPath];
        indexPathToScroll = [indexPathToScroll indexPathByAddingIndex:1];
        
        [collectionView scrollToItemAtIndexPath:indexPathToScroll
                               atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                       animated:YES];
    }
    else if (pointTo.y < ([collectionView contentOffset].y + [cellTo bounds].size.height / 2))
    {
        NSIndexPath *indexPathToScroll = [self getMinVisibleIndexPath];
        indexPathToScroll = [indexPathToScroll indexPathByAddingIndex:-1];
        
        if ([indexPathToScroll item] > 0)
        {
            [collectionView scrollToItemAtIndexPath:indexPathToScroll
                                   atScrollPosition:UICollectionViewScrollPositionCenteredVertically
                                           animated:YES];
        }
    }
    
    if (![indexFrom isEqual:indexTo])
    {
        MMMPlayTrackCell *cellTo = (MMMPlayTrackCell *)[collectionView cellForItemAtIndexPath:indexTo];
        
        if (pointTo.y < ([cellTo center].y))
        {
            [collectionView moveItemAtIndexPath:indexFrom toIndexPath:indexTo];
            
            NSInteger from = [indexFrom item];
            NSInteger into = [indexTo item];
            
            NSMutableArray *mutArray = [NSMutableArray arrayWithArray:[self modelsList]];
            MMMTrack *draggedTrack = [mutArray objectAtIndex:from];
            [mutArray removeObject:draggedTrack];
            [mutArray insertObject:draggedTrack atIndex:into];
            
            [self updateViewModelsList:(NSArray *)mutArray];
        }
    }
}

- (void)saveTrackList
{
    MLOG(@"%@", [[[self modelsList] firstObject] songTitle]);
    [[self delegate] saveTrackList:[self modelsList]];
}

- (void)deleteTrackByIndex:(NSIndexPath *)indexPath;
{
    MMMDisplayingCellModel *displayingModel = [[self viewModelsList] objectAtIndex:[indexPath item]];
    MMMTrack *track = [self modelByViewModel:displayingModel];
    
    if (track != nil)
    {
        [[self delegate] deleteTrack:track];
    }
}

#pragma mark - private

- (NSIndexPath *)getMaxVisibleIndexPath
{
    NSArray *cells = [[self collectionView] visibleCells];
    NSIndexPath *index = [NSIndexPath indexPathWithIndex:0];
    
    for (MMMPlayTrackCell *cell in cells)
    {
        NSIndexPath *index2 = [[self collectionView] indexPathForCell:cell];
        
        if ([index2 compare:index] == NSOrderedDescending)
        {
            index = index2;
        }
    }

    return index;
}

- (NSIndexPath *)getMinVisibleIndexPath
{
    NSArray *cells = [[self collectionView] visibleCells];
    NSIndexPath *index = [NSIndexPath indexPathWithIndex:[[self collectionView] numberOfItemsInSection:0]];
    
    for (MMMPlayTrackCell *cell in cells)
    {
        NSIndexPath *index2 = [[self collectionView] indexPathForCell:cell];
        
        if ([index2 compare:index] == NSOrderedAscending)
        {
            index = index2;
        }
    }
    
    return index;
}

//#pragma mark - UICollectionViewDelegate
//
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return YES;
//}
//
//- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
//{
//    if ([NSStringFromSelector(action) isEqualToString:@"delete:"])
//        return YES;
//    else
//        return NO;
//}
//
//- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
//{
//    if ([NSStringFromSelector(action) isEqualToString:@"delete:"])
//    {
//        [[self delegate] deleteTrackByIndex:[indexPath item]];
//    }
//}

@end