//
//  MMMFileSystemResolver.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/11/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMFileSystemResolver.h"
#import "MMMTrack.h"

@interface MMMFileSystemResolver ()

@property NSString *appDocDir;
@property NSString *resourcesDirectory;
@property NSString *cacheDirectory;

@end

@implementation MMMFileSystemResolver

static NSString *const resourcesDir = @"resources";
static NSString *const cacheDir = @"cache";

+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _appDocDir = [[MMMFileSystemResolver applicationDocumentsDirectory] path];
        _resourcesDirectory = [self checkAndCreateDirectory:resourcesDir];
        _cacheDirectory = [self checkAndCreateDirectory:cacheDir];
    }
    
    return self;
}

- (NSString *)checkAndCreateDirectory:(NSString *)dirName
{
    NSString *directory = [_appDocDir stringByAppendingPathComponent:dirName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir = YES;
    
    if (![fileManager fileExistsAtPath:directory isDirectory:&isDir])
    {
        NSError *error = nil;
        [fileManager createDirectoryAtPath:directory withIntermediateDirectories:NO attributes:nil error:&error];
        
        if (error)
        {
            MLOG(@"create dir error:\n%@", [error localizedDescription]);
            directory = _appDocDir;
        }
    }
    
    return directory;
}

- (void)saveTrackResources:(MMMTrack *)track
{
    if ([track artworkImage])
    {
        NSString *fileName = [[track artworkImageURL] lastPathComponent];
        NSString *saveLocation = [self getFullResourceFilePath:fileName];
        
        NSError *error = nil;
        [UIImagePNGRepresentation([track artworkImage]) writeToFile:saveLocation options:NSDataWritingAtomic error:&error];
        
        if (error != nil)
        {
            MLOG(@"writePictureError - %@", error);
            [track setArtworkImageFilePath:@""];
        }
        else
        {
            [track setArtworkImageFilePath:[self getShortResourceFilePath:fileName]];
        }
    }
    
    if ([track waveformImage])
    {
        NSString *fileName = [[track waveformImageURL] lastPathComponent];
        NSString *saveLocation = [self getFullResourceFilePath:fileName];
        
        NSError *error = nil;
        [UIImagePNGRepresentation([track waveformImage]) writeToFile:saveLocation options:NSDataWritingAtomic error:&error];
        
        if (error != nil)
        {
            MLOG(@"writeWaveformError - %@", error);
            [track setWaveformImageFilePath:@""];
        }
        else
        {
            [track setWaveformImageFilePath:[self getShortResourceFilePath:fileName]];
        }
    }
}

- (void)deleteTrackResources:(MMMTrack *)track
{
    [self deleteResource:[track artworkImageFilePath]];
    [self deleteResource:[track waveformImageFilePath]];
    [self deleteResource:[track songFilePath]];
}

- (void)deleteResource:(NSString *)path
{
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if ([path length] > 0 && path != nil)
    {
        [fileManager removeItemAtPath:[self getFullFilePath:path] error:&error];
        
        if (error != nil)
        {
            MLOG(@"deleteResourceError - %@", error);
        }
    }
}

- (NSError *)saveSongData:(NSData *)data forTrack:(MMMTrack *)track
{
    NSString *name = [NSString stringWithFormat:@"%@_%@.mp3", [track sourceMedia], [track trackId]];
    NSString *saveLocation = [self getFullResourceFilePath:name];
    
    NSError *error = nil;
    [data writeToFile:saveLocation options:NSDataWritingAtomic error:&error];
    
    if (error == nil)
    {
        [track setSongFilePath:[self getShortResourceFilePath:name]];
    }
    
    return error;
}

- (NSString *)saveToCacheImage:(UIImage *)image withFileName:(NSString *)fileName
{
    NSString *saveLocation = [self getFullCacheFilePath:fileName];
    
    NSError *error = nil;
    [UIImagePNGRepresentation(image) writeToFile:saveLocation options:NSDataWritingAtomic error:&error];
    
    if (error != nil)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            MLOG(@"save cache error - %@", error);
        });

        return @"";
    }
    else
    {
        return [self getShortCacheFilePath:fileName];
    }
}

- (void)clearCache
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    BOOL success = [fileManager removeItemAtPath:_cacheDirectory error:&error];
    
    if (!success || error)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            MLOG(@"clear cache error - %@", error);
        });
    }
    else
    {
        [self checkAndCreateDirectory:cacheDir];
    }
}

- (NSString *)getFullFilePath:(NSString *)shortPath
{
    return [_appDocDir stringByAppendingPathComponent:shortPath];
}

- (NSString *)getFullResourceFilePath:(NSString *)fileName
{
    return [_resourcesDirectory stringByAppendingPathComponent:fileName];
}

- (NSString *)getShortResourceFilePath:(NSString *)fileName
{
    return [resourcesDir stringByAppendingPathComponent:fileName];
}

- (NSString *)getFullCacheFilePath:(NSString *)fileName
{
    return [_cacheDirectory stringByAppendingPathComponent:fileName];
}

- (NSString *)getShortCacheFilePath:(NSString *)fileName
{
    return [cacheDir stringByAppendingPathComponent:fileName];
}

@end
