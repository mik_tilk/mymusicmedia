//
//  MMMFileSystemResolver.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/11/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMMTrack;

@interface MMMFileSystemResolver : NSObject

+ (NSURL *)applicationDocumentsDirectory;

- (void)saveTrackResources:(MMMTrack *)track;
- (void)deleteTrackResources:(MMMTrack *)track;
- (NSError *)saveSongData:(NSData *)data forTrack:(MMMTrack *)track;
- (NSString *)saveToCacheImage:(UIImage *)data withFileName:(NSString *)fileName;
- (void)clearCache;
- (NSString *)getFullFilePath:(NSString *)shortPath;
- (NSString *)getShortResourceFilePath:(NSString *)fileName;

@end
