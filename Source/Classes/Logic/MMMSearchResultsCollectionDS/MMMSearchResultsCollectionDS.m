//
//  MMMSearchResultsCollectionDS.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionDS_Protected.h"
#import "MMMSearchResultsCollectionDS.h"
#import "MMMSearchTrackCell.h"

@implementation MMMSearchResultsCollectionDS

#pragma mark - UICollectionViewDataSource

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        identifier = NSStringFromClass([MMMSearchTrackCell class]);
    });
    
    MMMSearchTrackCell *cell = (MMMSearchTrackCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                               forIndexPath:indexPath];
    
    MMMDisplayingCellModel *viewModel = [[self viewModelsList] objectAtIndex:[indexPath item]];
    [cell setupWithDisplayingModel:viewModel];

    [cell setDidCellTapHandler:^(MMMBaseCell *cell)
    {
        [[self delegate] willPlayTrack:[self modelByViewModel:viewModel]];
    }];
    
    [cell setDidWaveformPanHandler:^(CGFloat position, MMMBaseCell *cell)
    {
        [[self delegate] seekPlayPosition:position forTrack:[self modelByViewModel:viewModel]];
    }];

    [cell setDidAddButtonTapHandler:^(MMMSearchTrackCell *cell)
    {
        [[self delegate] willAddTrack:[self modelByViewModel:viewModel]];
    }];
   
    return cell;
}

#pragma mark - MMMTrackListControllerDelegateProtocol

- (void)updateVisibleCellFromTrack:(MMMTrack *)track byIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    MMMDisplayingCellModel *displayingModel = [[self viewModelsList] objectAtIndex:index];
    MMMBaseCell *cell = (MMMBaseCell *)[[self collectionView] cellForItemAtIndexPath:indexPath];
    
    [displayingModel updateViewModelImages:track];
    
    [cell updateCellByDisplayingModel:displayingModel];
}

- (void)resignFirstResponder
{
    [[[self collectionView] superview] resignFirstResponder];
}

@end