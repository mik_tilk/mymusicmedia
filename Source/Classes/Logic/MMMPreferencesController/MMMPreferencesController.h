//
//  MMMPreferencesController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MMMRequestPreferencesProtocol.h"
#import "MMMPreferencesDSDelegateProtocol.h"

@protocol MMMPreferencesDSDelegateProtocol;
@protocol MMMPreferencesListDelegateProtocol;
@protocol MMMRequestPreferencesProtocol;

@class MMMDataSourceController;
@class MMMPlayController;

@interface MMMPreferencesController : NSObject<MMMPreferencesDSDelegateProtocol, MMMRequestPreferencesProtocol>

@property(nonatomic, weak) id<MMMPreferencesListDelegateProtocol> delegate;

- (instancetype)initWithDataSource:(MMMDataSourceController *)dsController andPlayer:(MMMPlayController *)player;
- (void)loadPreferences;
- (void)updateCollectionDS;

@end