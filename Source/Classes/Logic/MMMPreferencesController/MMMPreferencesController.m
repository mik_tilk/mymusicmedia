 //
//  MMMPreferencesController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPreferencesListDelegateProtocol.h"
#import "MMMPreferencesController.h"
#import "MMMDataSourceController.h"
#import "MMMProperties.h"
#import "MMMPlayController.h"

@interface MMMPreferencesController ()

@property(nonatomic, strong) MMMDataSourceController *dsController;
@property(nonatomic, strong) MMMPlayController *player;
@property(nonatomic, copy) MMMProperties *properties;

@end

@implementation MMMPreferencesController

- (instancetype)initWithDataSource:(MMMDataSourceController *)dsController andPlayer:(MMMPlayController *)player
{
    self = [super init];
    
    if (self != nil)
    {
        _dsController = dsController;
        _player = player;

        [_dsController setDelegatePreferenceUpdate:self];
        [_player setPreferences:self];
    }
    
    return self;
}

- (void)loadPreferences
{
    [self setProperties:[_dsController loadProperties]];
}

- (void)updateCollectionDS
{
    [_delegate propertiesDidChanged:_properties];
}

#pragma mark - MMMPreferencesDSDelegateProtocol

- (void)didChangeProperty:(NSString *)property
{
    NSNumber *value = [[_properties propertiesList] objectForKey:property];
    NSNumber *value2 = @(1 - value.integerValue);
    [_properties setProperty:property toValue:value2];
    [_dsController saveProperties:_properties];
    [_delegate propertiesDidChanged:_properties];
}

#pragma mark - MMMRequestPreferencesProtocol

- (BOOL)mustContinueFromBegining
{
    return [[[_properties propertiesList] valueForKey:@"Continue from begining"] boolValue];
}

- (BOOL)isAllowedToDownload
{
    BOOL useOnlyWiFi = [[[_properties propertiesList] valueForKey:@"Only by WiFi"] boolValue];
    
    if (!useOnlyWiFi)
    {
        return YES;
    }
    else if (useOnlyWiFi && [self isWiFiConnected])
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)mustLoadImage
{
    return [[[_properties propertiesList] valueForKey:@"Load images"] boolValue];
}

#pragma mark - private

- (BOOL)isWiFiConnected
{
        //    return [self wifiDetails] == nil ? NO : YES;
    BOOL hasWiFiNetwork = NO;
    NSArray *interfaces = CFBridgingRelease(CNCopySupportedInterfaces());
    
    for (NSString *interface in interfaces)
    {
        NSDictionary *networkInfo = CFBridgingRelease(CNCopyCurrentNetworkInfo((__bridge CFStringRef)(interface)));
        
        if (networkInfo != NULL)
        {
            hasWiFiNetwork = YES;
            break;
        }
    }
    
    return hasWiFiNetwork;
}

@end
