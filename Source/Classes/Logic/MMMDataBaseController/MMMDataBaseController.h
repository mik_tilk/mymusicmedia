//
//  MMMDataBaseController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@class MMMTrack;
@class MMMProperties;

@interface MMMDataBaseController : NSObject

- (NSMutableArray<MMMTrack *> *)readTracklistFromDB;
- (MMMProperties *)readPropertiesFromDB;
- (void)savePropertiesToDB:(MMMProperties *)properties;
- (void)addTrackToDB:(MMMTrack *)track;
- (void)savePlayListToDB:(NSArray<MMMTrack *> *)playList;
- (void)deleteTrackFromDB:(MMMTrack *)track;
- (BOOL)saveSongData:(NSData *)data forTrack:(MMMTrack *)track;
- (void)clearCache;

@end
