//
//  MMMDataBaseController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMDataBaseController.h"
#import "MMMFileSystemResolver.h"
#import "MMMProperties.h"
#import "MMMTrack.h"
#import "MMMdbProperties.h"
#import "MMMdb.h"

@interface MMMDataBaseController ()

@property(nonatomic, copy) NSArray *db;
@property(nonatomic, copy) MMMFileSystemResolver *fileResolver;
@property(nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic, strong) NSFetchRequest *request;

@end

@implementation MMMDataBaseController

static NSString *const databaseCoreDataModel = @"MMMdbModel";
static NSString *const databaseFileName = @"MMMDB.sqlite";
static NSString *const entityTracksDBName = @"MMMdb";
static NSString *const entityPropertiesDBName = @"MMMdbProperties";

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        _db = [NSArray array];
        _request = [NSFetchRequest new];
        _fileResolver = [MMMFileSystemResolver new];
    }
    
    return self;
}

- (NSMutableArray<MMMTrack *> *)readTracklistFromDB
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityTracksDBName inManagedObjectContext:context];
    [_request setEntity:entity];
    _db = [context executeFetchRequest:_request error:nil];
    
    NSMutableArray *trackList = [NSMutableArray array];
    
    for (MMMdb *dbTrack in _db)
    {
        MMMTrack *track = [dbTrack model];
        
        if ([[track artworkImageFilePath] length] > 0)
        {
            [track setArtworkImage:[UIImage imageWithContentsOfFile:[_fileResolver getFullFilePath:[track artworkImageFilePath]]]];
        }
        
        [track setWaveformImage:[UIImage imageWithContentsOfFile:[_fileResolver getFullFilePath:[track waveformImageFilePath]]]];

        [trackList addObject:track];
    }
    
    return trackList;
}

- (MMMProperties *)readPropertiesFromDB
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityPropertiesDBName inManagedObjectContext:context];
    [_request setEntity:entity];
    _db = [context executeFetchRequest:_request error:nil];
    
    MMMProperties *properties = [MMMProperties new];
    
    if ([_db count] != 0)
    {
        properties = [(MMMdbProperties *)[_db firstObject] model];
    }
    else
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        MMMdbProperties *dbProperties = [NSEntityDescription insertNewObjectForEntityForName:entityPropertiesDBName
                                                                      inManagedObjectContext:context];
        [dbProperties mapPropertiesToDB:properties];
        
        [self saveContext];
    }
    
    return properties;
}

- (void)savePropertiesToDB:(MMMProperties *)properties
{
    NSError *error;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[NSEntityDescription entityForName:entityPropertiesDBName inManagedObjectContext:_managedObjectContext]];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSManagedObject *record = [results firstObject];
    
    [record setValue:([properties propertiesList]) forKey:NSStringFromSelector(@selector(propertiesList))];
    
    [self saveContext];
}

- (void)addTrackToDB:(MMMTrack *)track
{
    [_fileResolver saveTrackResources:track];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    MMMdb *dbTrack = [NSEntityDescription insertNewObjectForEntityForName:entityTracksDBName inManagedObjectContext:context];
    
    [dbTrack mapTrackToDB:track];
        
    [self saveContext];
    MLOG(@"Added : %@", [dbTrack valueForKey:@"trackId"]);
}

- (void)savePlayListToDB:(NSArray<MMMTrack *> *)playList
{
    MLOG(@"%@", [[playList firstObject] songTitle]);

    NSManagedObjectContext *context = [self managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityTracksDBName inManagedObjectContext:context];
    [_request setEntity:entity];
    _db = [context executeFetchRequest:_request error:nil];
    
    for (MMMdb *dbTrack in _db)
    {
        [_managedObjectContext deleteObject:dbTrack];
        
        [self saveContext];
    }

    for (MMMTrack *track in playList)
    {
//        NSManagedObjectContext *context = [self managedObjectContext];
        MMMdb *dbTrack = [NSEntityDescription insertNewObjectForEntityForName:entityTracksDBName inManagedObjectContext:context];
        
        [dbTrack mapTrackToDB:track];
        
        [self saveContext];
    }

    MLOG(@"saved List");
}

- (BOOL)saveSongData:(NSData *)data forTrack:(MMMTrack *)track
{
    NSError *error = [_fileResolver saveSongData:data forTrack:track];
    
    if (error != nil)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            MLOG(@"writeWaveformError - %@", error);
        });

        return NO;
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"trackId == %@", [track trackId]];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:[NSEntityDescription entityForName:entityTracksDBName inManagedObjectContext:_managedObjectContext]];
        [request setPredicate:predicate];
        
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        
        NSManagedObject *record = [results objectAtIndex:0];
        
        [record setValue:[track songFilePath] forKey:NSStringFromSelector(@selector(songFilePath))];
        
        [self saveContext];

        return YES;
    }
}

- (void)deleteTrackFromDB:(MMMTrack *)track
{
    [_fileResolver deleteTrackResources:track];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    for (MMMdb *dbTrack in _db)
    {
        if ([dbTrack valueForKey:@"trackId"] == [track trackId])
        {
            [context deleteObject:dbTrack];
            break;
        }
    }
    
    [self saveContext];
    MLOG(@"removed : %@", [track trackId]);
}

- (void)clearCache
{
    [_fileResolver clearCache];
}

#pragma mark - CoreData

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)
    {
        return _managedObjectModel;
    }
    
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:databaseCoreDataModel withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    MLOG(@"%@", [MMMFileSystemResolver applicationDocumentsDirectory]);
    
    NSURL *storeURL = [[MMMFileSystemResolver applicationDocumentsDirectory] URLByAppendingPathComponent:databaseFileName];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        MLOG(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    
    if (coordinator != nil)
    {
        _managedObjectContext = [NSManagedObjectContext new];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            MLOG(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
