//
//  MMMTrackListController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/5/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMBaseListController.h"

@interface MMMSearchListController : MMMBaseListController<UISearchBarDelegate>

- (void)doSearch;
- (void)searchText:(NSString *)text withCompletion:(void (^)(NSArray<MMMTrack *> *searchResultsList, NSError *error))completion;

@end
