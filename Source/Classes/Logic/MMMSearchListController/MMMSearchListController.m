//
//  MMMTrackListController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/5/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseListController_Protected.h"
#import "MMMSearchListController.h"

@interface MMMSearchListController ()

@property(nonatomic, copy) UIAlertView *allert;
@property(nonatomic, copy) NSString *searchText;
@property(nonatomic, strong) NSTimer *searchTimer;

@end

@implementation MMMSearchListController

static const NSTimeInterval searchInterval = 1;
static const NSInteger startSearchMinLenght = 2;

#pragma mark - MUST BE REDEFINED

- (void)setDataSourceDelegate
{
    _allert = nil;
    [[self dsController] setDelegateSearchListUpdate:self];
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
{
    if ([searchText length] < startSearchMinLenght)
    {
        [_searchTimer invalidate];
        _searchTimer = nil;
        
        return;
    }
    
    if (_searchTimer != nil)
    {
        [_searchTimer invalidate];
        _searchTimer = nil;
    }

    _searchText = [searchBar text];
    _searchTimer = [NSTimer scheduledTimerWithTimeInterval:searchInterval target:self selector:@selector(doSearch) userInfo:nil repeats:NO];
    
    NSRunLoop *runner = [NSRunLoop currentRunLoop];
    [runner addTimer:_searchTimer forMode:NSDefaultRunLoopMode];
}

#pragma mark - .h

- (void)doSearch
{
    [_searchTimer invalidate];
    _searchTimer = nil;

    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    
    [self searchText:_searchText withCompletion:^(NSArray<MMMTrack *> *searchResults, NSError *error)
    {
        if (error)
        {
            _allert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                 message:error.localizedDescription
                                                delegate:nil
                                       cancelButtonTitle:@"OK. I've gotit"
                                       otherButtonTitles:nil];
        }
        else
        {
            [self setTrackList:searchResults];
        }
        
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^
    {
        if (_allert != nil)
        {
            [[self delegate] resignFirstResponder];
            [_allert show];
            _allert = nil;
        }
        else
        {
            if ([self isPlaying])
            {
//                [[self player] stop];
                [self player:[self player] didStopPlayTrack:nil];
            }
            
            [[self delegate] resignFirstResponder];
            [[self delegate] trackListDidChanged:[self trackList]];
        }
    });
}

- (void)searchText:(NSString *)text withCompletion:(void (^)(NSArray<MMMTrack *> *trackList, NSError *error))completion
{
    [[self dsController] searchText:(NSString *)text withCompletion:^(NSArray *searchResultsList, NSError *error)
    {
        if (error)
        {
            completion(nil, error);
        }
        else
        {
            [self setTrackList:searchResultsList];
            completion([self trackList], nil);
        }
    }];
}

#pragma mark - MMMCollectionDSDelegateProtocol

- (void)willAddTrack:(MMMTrack *)track
{
    [[self dsController] addTrack:track];
}

- (void)trackDidChanged:(MMMTrack *)track
{
    if ([[self trackList] containsObject:track])
    {
        [[self delegate] updateVisibleCellFromTrack:(MMMTrack *)track byIndex:[[self trackList] indexOfObject:track]];
    }
}

@end