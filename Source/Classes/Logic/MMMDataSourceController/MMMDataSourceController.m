//
//  MMMDataSourceController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//
//        /Users/admin/Library/Developer/CoreSimulator/Devices/1C3940A5-2FBD-4B5C-9B2B-997830364ECC/data/Containers/Data/Application/8DCF0EEF-C2B3-4311-A8B4-4BCA8FDEB42B/Documents

#import "MMMCollectionDSDelegateProtocol.h"
#import "MMMPreferencesDSDelegateProtocol.h"
#import "MMMRequestPreferencesProtocol.h"
#import "MMMRequestPreferencesProtocol.h"
#import "MMMDownloadDelegateProtocol.h"
#import "MMMDataSourceController.h"
#import "MMMModelAdapter.h"
#import "MMMDataBaseController.h"
#import "MMMDownloadResourcesManager.h"
#import "MMMSoundcloudRequestManager.h"
#import "MMMTrack.h"
#import "MMMProperties.h"
#import <ifaddrs.h>
#import <net/if.h>
#import <SystemConfiguration/CaptiveNetwork.h>

@interface MMMDataSourceController ()<MMMDownloadDelegateProtocol>

@property(nonatomic, strong) NSArray<MMMTrack *> *playlist;
@property(nonatomic, strong) MMMProperties *properties;
@property(nonatomic, strong) NSArray<MMMTrack *> *searchResultsList;

@property(nonatomic, strong) MMMModelAdapter *adapter;
@property(nonatomic, strong) MMMSoundcloudRequestManager *soundcloudManager;
@property(nonatomic, strong) MMMDataBaseController *dbController;
@property(nonatomic, strong) MMMDownloadResourcesManager *downloadManager;

@end

@implementation MMMDataSourceController

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        _downloadManager = [MMMDownloadResourcesManager new];
        [_downloadManager setDelegate:self];
        
        _adapter = [MMMModelAdapter new];
        [_adapter setDataSourceDelegate:self];
        
        _soundcloudManager = [MMMSoundcloudRequestManager new];
        
        _dbController = [MMMDataBaseController new];

        _searchResultsList = [NSArray array];
    }
    
    return self;
}

- (NSArray *)loadSavedTracklist
{
    _playlist = [_dbController readTracklistFromDB];
    
    return (NSArray *)_playlist;
}

- (MMMProperties *)loadProperties
{
    _properties = [_dbController readPropertiesFromDB];
    
    return _properties;
}

- (void)saveProperties:(MMMProperties *)properties
{
    [_dbController savePropertiesToDB:properties];
}

- (void)addTrack:(MMMTrack *)track
{
    if (![_playlist containsObject:track])
    {
        MLOG(@"track size - %lu", (unsigned long)[track size]);
        [self downloadSongForTrack:track];

        [_dbController addTrackToDB:track];
        NSMutableArray *mutableList = [NSMutableArray arrayWithArray:_playlist];
        [mutableList addObject:track];
        _playlist = [NSArray arrayWithArray:mutableList];
        
        [_delegatePlayListUpdate trackListDidChanged:_playlist];
    }
}

- (void)savePlayList:(NSArray<MMMTrack *> *)playList
{
    [_dbController savePlayListToDB:playList];
    _playlist = playList;
}

- (void)downloadSongForTrack:(MMMTrack *)track
{
    if ([_delegatePreferenceUpdate isAllowedToDownload])
    {
        [_downloadManager downloadSongByURL:[_soundcloudManager getTrackStream:[track streamURL]] forTrack:track];
    }
}

- (void)deleteTrack:(MMMTrack *)track
{
    if ([_playlist containsObject:track])
    {
        NSMutableArray *mutableList = [NSMutableArray arrayWithArray:_playlist];
        [mutableList removeObject:track];
        _playlist = [NSArray arrayWithArray:mutableList];
        
        [_dbController deleteTrackFromDB:track];
        [self playListDidChanged];
    }
}

- (void)searchText:(NSString *)text withCompletion:(void (^)(NSArray<MMMTrack *> *searchResultsList, NSError *error))completion
{
    [_soundcloudManager searchText:text withCompletion:^(NSArray *jsonTrackList, NSError *error)
    {
        if (error != nil)
        {
            completion(nil, error);
        }
        else
        {
            [[self dbController] clearCache];
            NSMutableArray *searchResultsList = [NSMutableArray array];
            
            for (NSDictionary *jsonTrack in jsonTrackList)
            {
                [searchResultsList addObject:[_adapter makeTrackFromSoundcloud:jsonTrack]];
            }

            _searchResultsList = (NSArray<MMMTrack *> *)searchResultsList;
            
            if ([_delegatePreferenceUpdate mustLoadImage])
            {
                if ([_delegatePreferenceUpdate isAllowedToDownload])
                {
                    [_downloadManager loadListResourcesInBackground:_searchResultsList];
                }
//                else
//                {
//                    [_downloadManager loadListResourcesInBackground:_searchResultsList];
//                }
            }
            
            completion(_searchResultsList, nil);
        }
    }];
}

- (void)playListDidChanged
{
    [_delegatePlayListUpdate trackListDidChanged:_playlist];
}

- (void)searchResultsListDidChanged
{
    [_delegateSearchListUpdate trackListDidChanged:_searchResultsList];
}

#pragma mark - MMMDownloadDelegateProtocol

- (void)searchResultsListDidChangedTrack:(MMMTrack *)track
{
    [_delegateSearchListUpdate trackDidChanged:track];
}

- (void)didDownloadSongData:(NSData *)data forTrack:(MMMTrack *)track
{
    [_dbController saveSongData:data forTrack:track];
}

@end
