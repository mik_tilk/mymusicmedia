//
//  MMMDataSourceController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMMCollectionDSDelegateProtocol;
@protocol MMMPreferencesDSDelegateProtocol;
@protocol MMMRequestPreferencesProtocol;

@class MMMTrack;
@class MMMProperties;

@interface MMMDataSourceController : NSObject

@property(nonatomic, weak) id<MMMCollectionDSDelegateProtocol> delegatePlayListUpdate;
@property(nonatomic, weak) id<MMMCollectionDSDelegateProtocol> delegateSearchListUpdate;
@property(nonatomic, weak) id<MMMPreferencesDSDelegateProtocol, MMMRequestPreferencesProtocol> delegatePreferenceUpdate;

- (NSArray *)loadSavedTracklist;
- (MMMProperties *)loadProperties;
- (void)saveProperties:(MMMProperties *)properties;
- (void)searchText:(NSString *)text withCompletion:(void (^)(NSArray<MMMTrack *> *trackList, NSError *error))completion;
- (void)addTrack:(MMMTrack *)track;
- (void)savePlayList:(NSArray *)playList;
- (void)deleteTrack:(MMMTrack *)track;
- (void)downloadSongForTrack:(MMMTrack *)track;

@end
