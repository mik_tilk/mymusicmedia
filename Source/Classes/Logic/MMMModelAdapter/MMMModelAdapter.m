//
//  MMMModelAdapter.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMModelAdapter.h"
#import "MMMTrack.h"
#import "NSDictionary+NonNull.h"

@implementation MMMModelAdapter

static NSString *const trackId = @"id";
static NSString *sourceMedia = @"";
static NSString *const title = @"title";
static NSString *const duration = @"duration";
static NSString *const pictureURL = @"artwork_url";
static NSString *const streamURL = @"stream_url";
static NSString *const waveformURL = @"waveform_url";
static NSString *const downloadable = @"downloadable";
static NSString *const size = @"original_content_size";

- (MMMTrack *)makeTrackFromSoundcloud:(NSDictionary *)response
{
    sourceMedia = @"soundCloud";

    MMMTrack *track = [MMMTrack new];
    
    if (track)
    {
        [track setTrackId:[NSString stringWithFormat:@"%@", response[trackId]]];
        [track setLocalId:[[NSUUID UUID] UUIDString]];
        [track setSourceMedia:sourceMedia];
        [track setDuration:[response[duration] integerValue]];
        [track setArtworkImage:nil];
        [track setArtworkImageURL:[response nonNullObjectForKey:pictureURL]];
        [track setArtworkImageFilePath:@""];
        [track setSongFilePath:@""];
        [track setStreamURL:[response nonNullObjectForKey:streamURL]];
        [track setWaveformImage:nil];
        [track setWaveformImageURL:[response nonNullObjectForKey:waveformURL]];
        [track setWaveformImageFilePath:@""];
        [track setDownloadable:[[response nonNullObjectForKey:downloadable] boolValue]];
        [track setSize:[[response nonNullObjectForKey:size] integerValue]];
        
        [self setArtistAndSongName:response[title] forTrack:track];
    }
    
    return track;
}

- (void)setArtistAndSongName:(NSString *)title forTrack:(MMMTrack *)track
{
    NSString *artist = @"";
    NSString *songTitle = @"";
    NSArray *arr = [title componentsSeparatedByString:@" - "];
    
    NSInteger count = [arr count];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];

    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSNumber *firstWordAsNumber = [formatter numberFromString:[arr firstObject]];
    
    if (firstWordAsNumber == nil && count > 1)
    {
        artist = [NSString stringWithFormat:@"%@", [arr firstObject]];
        NSMutableArray *mutArr = [NSMutableArray arrayWithArray:arr];
        [mutArr removeObjectAtIndex:0];
        songTitle = [mutArr componentsJoinedByString:@" - "];
    }
    else
    {
        songTitle = [NSString stringWithFormat:@"%@", title];
    }
    
    [track setArtist:artist];
    [track setSongTitle:songTitle];
}

@end
