//
//  ModelAdapter.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMMTrack;

@interface MMMModelAdapter : NSObject

@property(nonatomic, weak) id dataSourceDelegate;

- (MMMTrack *)makeTrackFromSoundcloud:(NSDictionary *)response;

@end
