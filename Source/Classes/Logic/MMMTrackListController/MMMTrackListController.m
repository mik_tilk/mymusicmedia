//
//  MMMTrackListController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/5/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMTrackListController.h"
#import "MMMDataSourceController.h"

@implementation MMMTrackListController

@synthesize delegate;

- (void)initWithCompletion:(void (^)(MMMTrackListController *controller, NSError *error))completion
{
    __weak typeof(self) weakSelf = self;
    
    [MMMDataSourceController createTrackListWithCompletion:^(NSMutableArray *trackList, NSError *error)
    {
        if (error)
        {
            NSLog(@"MMMTrackListController- %@", error);
            completion(nil, error);
        }
        else
        {
            [weakSelf setTrackList:trackList];
            NSLog(@"MMMTrackListController - %@", [[[weakSelf trackList] firstObject] trackId]);
            
            [[weakSelf delegate] trackListDidChanged:weakSelf];
            
            completion(weakSelf, nil);
        }
    }];
}

- (void)didAddTrack:(MMMTrackListController *)controller
{
//    --
}

@end
