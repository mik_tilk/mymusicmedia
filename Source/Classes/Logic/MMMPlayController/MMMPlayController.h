//
//  MMMPlayController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@protocol MMMPlayerDelegateProtocol, MMMRequestPreferencesProtocol;

@class MMMTrack;

@interface MMMPlayController : NSObject

- (void)setDelegate:(id<MMMPlayerDelegateProtocol>)controller;
- (void)setPreferences:(id<MMMRequestPreferencesProtocol>)controller;
- (void)setTrack:(MMMTrack *)track;
- (void)seekPlayPosition:(CGFloat)position;
- (void)playPause;
- (void)play;
- (void)pause;
- (void)stop;

@end
