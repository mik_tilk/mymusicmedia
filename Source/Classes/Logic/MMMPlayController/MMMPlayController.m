//
//  MMMPlayController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/2/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMRequestPreferencesProtocol.h"
#import "MMMPlayerDelegateProtocol.h"
#import "MMMPlayController.h"
#import "MMMSoundcloudRequestManager.h"
#import "MMMFileSystemResolver.h"
#import "MMMCellState.h"
#import "MMMTrack.h"

@interface MMMPlayController ()

@property(nonatomic, assign) id<MMMPlayerDelegateProtocol> delegate;
@property(nonatomic, assign) id<MMMRequestPreferencesProtocol> preferences;
@property(nonatomic, strong) AVPlayer *player;
@property(nonatomic, strong) MMMTrack *playingTrack;
@property(nonatomic, strong) AVPlayerItem *playingItem;
@property(nonatomic, strong) MMMSoundcloudRequestManager *soundcloudManager;
@property(nonatomic, strong) MMMFileSystemResolver *fileResolver;
@property(nonatomic, assign) id timeObserver;
@property(nonatomic, assign) BOOL observing;

@end

@implementation MMMPlayController

static void *statusObserverContext = &statusObserverContext;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        _soundcloudManager = [MMMSoundcloudRequestManager new];
        _fileResolver = [MMMFileSystemResolver new];
        _delegate = nil;
        _playingItem = [AVPlayerItem new];
        _timeObserver = nil;
        _observing = NO;
    }
    
    return self;
}

- (void)setDelegate:(id<MMMPlayerDelegateProtocol>)controller
{
    if (_delegate != nil && _delegate != controller)
    {
        [_delegate player:self didStopPlayTrack:_playingTrack];
    }
    
    _delegate = controller;
}

- (void)setPreferences:(id<MMMRequestPreferencesProtocol>)controller
{
    _preferences = controller;
}

- (void)setTrack:(MMMTrack *)track
{
    _playingTrack = track;
    
    if ([_playingTrack songFilePath] == nil || [[_playingTrack songFilePath] length] == 0)
    {
        NSString *currentPlayingURL = [_soundcloudManager getTrackStream:[_playingTrack streamURL]];
        MLOG(@"\nnew track - %@", [_playingTrack streamURL]);
        _playingItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:currentPlayingURL]];
    }
    else
    {
        NSString *currentPlayingURL = [_fileResolver getFullFilePath:[_playingTrack songFilePath]];
        MLOG(@"\nnew track - %@", [_playingTrack songFilePath]);
        _playingItem = [AVPlayerItem playerItemWithURL:[NSURL fileURLWithPath:currentPlayingURL]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:_playingItem];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playPause)
                                                 name:AVPlayerItemPlaybackStalledNotification
                                               object:_playingItem];
    
    _player = [AVPlayer playerWithPlayerItem:_playingItem];
    
    [_playingItem addObserver:self
                   forKeyPath:NSStringFromSelector(@selector(status))
                      options:NSKeyValueObservingOptionNew
                      context:&statusObserverContext];
    [self setObserving:YES];
}

- (void)seekPlayPosition:(CGFloat)position
{
    [_player seekToTime:CMTimeMakeWithSeconds(position, 60000)];
}

- (void)playPause
{
    if ([_player rate] == 0)
    {
        [_player play];
    }
    else
    {
        [_player pause];
    }
}

- (void)play
{
    [_player play];
    [_delegate player:self didChagedPlayingState:mPrepearingStreaming forTrack:_playingTrack];
}

- (void)pause
{
    [_player pause];
}

- (void)stop
{
//    [_player pause];
    
    if (_timeObserver != nil)
    {
        [_player removeTimeObserver:_timeObserver];
        _timeObserver = nil;
    }
    
    if (_observing)
    {
        [self setObserving:NO];
        [_playingItem removeObserver:self forKeyPath:NSStringFromSelector(@selector(status)) context:&statusObserverContext];
    }
    
    _player = nil;
}

#pragma mark - KVO methods

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    [_delegate player:self didStopPlayTrack:_playingTrack];
    [_delegate player:self didFinishedPlayTrack:_playingTrack];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _playingItem && [keyPath isEqualToString:NSStringFromSelector(@selector(status))])
    {
        if ([_playingItem status] == AVPlayerItemStatusFailed)
        {
            MLOG(@"\nAVPlayer Failed - %@\n", [_playingItem error]);
            
            [[[UIAlertView alloc] initWithTitle:@"Error"
                                        message:[[_playingItem error] localizedDescription]
                                       delegate:nil
                              cancelButtonTitle:@"OK. I've gotit"
                              otherButtonTitles:nil] show];

            _player = [AVPlayer new];
            [_delegate player:self didStopPlayTrack:_playingTrack];
        }
        else if ([_playingItem status] == AVPlayerItemStatusReadyToPlay)
        {
            [_delegate player:self didChagedPlayingState:mPlaying forTrack:_playingTrack];
            
            __weak id<MMMPlayerDelegateProtocol> weakDelegate = _delegate;
            __weak typeof(self) weakSelf = self;
            
            __weak MMMTrack *weakPlayingTrack = _playingTrack;
            
            _timeObserver = [_player addPeriodicTimeObserverForInterval:CMTimeMake(1, 10) queue:NULL usingBlock:^(CMTime time)
            {
                [weakDelegate player:weakSelf didUpdatePlayingProgress:(float)time.value / time.timescale forTrack:weakPlayingTrack];
            }];
        }
        else if ([_playingItem status] == AVPlayerItemStatusUnknown)
        {
            MLOG(@"AVPlayer Unknown\n");
            _player = [AVPlayer new];
            [_delegate player:self didStopPlayTrack:_playingTrack];
        }
    }
}

@end
