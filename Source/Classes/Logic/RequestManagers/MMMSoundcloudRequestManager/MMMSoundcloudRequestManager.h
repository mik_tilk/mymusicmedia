//
//  MMMSoundcloudRequestManager.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/1/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMMSoundcloudRequestManager : NSObject

- (void)trackListWithCompletion:(void (^)(NSArray *trackList, NSError *error))completion;
- (void)trackInfoByID:(NSString *)trackID withCompletion:(void (^)(NSDictionary *trackInfo, NSError *error))completion;
- (void)searchText:(NSString *)text withCompletion:(void (^)(NSArray *trackList, NSError *error))completion;
- (NSString *)getTrackStream:(NSString *)streamURL;

@end
