    //
    //  MMMSoundcloudRequestManager.m
    //  MyMusicMedia
    //
    //  Created by Kholomeev on 10/1/15.
    //  Copyright © 2015 NIX. All rights reserved.
    //

#import "AFHTTPRequestOperationManager.h"
#import "MMMSoundcloudRequestManager.h"
#import "MMMTrack.h"

@implementation MMMSoundcloudRequestManager

static NSString *const baseURL = @"https://api.soundcloud.com/tracks";
static NSString *const MMMclientID = @"b699ae28f59c77676146c22b6f3efa73";
static NSString *const MMMclientSecret = @"f80ab4f795f574608a64762e97519605";
static NSString *const keyForNextPageURL = @"linked_partitioning";
static NSString *const limit = @"150";

- (void)trackListWithCompletion:(void (^)(NSArray *trackList, NSError *error))completion
{
    NSString *url = [NSString stringWithFormat:@"%@?client_id=%@", baseURL, MMMclientID];
    
    [self performGetRequest:url withCompletion:^(id response, NSError *error)
    {
        if (error != nil)
        {
            completion(nil, error);
        }
        else if ([response isKindOfClass:[NSArray class]])
        {
            completion(response, nil);
        }
    }];
}

- (void)trackInfoByID:(NSString *)trackID withCompletion:(void (^)(NSDictionary *trackInfo, NSError *error))completion
{
    NSString *url = [NSString stringWithFormat:@"%@/%@?client_id=%@", baseURL, trackID, MMMclientID];
    
    [self performGetRequest:url withCompletion:^(id response, NSError *error)
    {
        if (error != nil)
        {
            completion(nil, error);
        }
        else if ([response isKindOfClass:[NSDictionary class]])
        {
            completion(response, nil);
        }
    }];
}

- (void)searchText:(NSString *)text withCompletion:(void (^)(NSArray *trackList, NSError *error))completion
{
    NSString *url = [NSString stringWithFormat:@"%@?client_id=%@&q=%@&limit=%@", baseURL, MMMclientID, text, limit];
    NSString *encodedURL = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [self performGetRequest:encodedURL withCompletion:^(id response, NSError *error)
    {
        if (error != nil)
        {
            completion(nil, error);
        }
        else if ([response isKindOfClass:[NSArray class]])
        {
            completion(response, nil);
        }
    }];
}

- (NSString *)getTrackStream:(NSString *)streamURL
{
    NSString *url = [NSString stringWithFormat:@"%@?client_id=%@&allow_redirects=False", streamURL, MMMclientID];
    
    return url;
}

#pragma mark - private

- (void)performGetRequest:(NSString *)url withCompletion:(void (^)(id response, NSError *error))completion
{
    AFHTTPRequestOperationManager *requestManager = [AFHTTPRequestOperationManager manager];
    [requestManager GET:url parameters:nil
                success:^(AFHTTPRequestOperation *operation, id responseObject)
                {
                    completion(responseObject, nil);
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error)
                {
                    completion(nil, error);
                }];
}

@end
