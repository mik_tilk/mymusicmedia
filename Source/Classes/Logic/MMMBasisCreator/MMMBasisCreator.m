//
//  MMMMainInterface.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBasisCreator.h"
#import "MMMBaseTabBarController.h"
#import "MMMDataSourceController.h"
#import "MMMPlayController.h"
#import "MMMPlayListController.h"
#import "MMMPlayPageVC.h"
#import "MMMSearchListController.h"
#import "MMMSearchPageVC.h"
#import "MMMPreferencesController.h"
#import "MMMPreferencePageVC.h"
#import "MMMCellPalette.h"

@interface MMMBasisCreator ()<UITabBarControllerDelegate, UIViewControllerAnimatedTransitioning>

@property MMMBaseTabBarController *tabBarController;

@end

@implementation MMMBasisCreator

- (void)setupBasicWindow:(UIWindow *)window
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillToggle:)
                                                 name:UIKeyboardWillHideNotification object:nil];

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:[NSBundle mainBundle]];
    MMMPlayPageVC *playPageVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MMMPlayPageVC class])];
    MMMSearchPageVC *searchPageVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MMMSearchPageVC class])];
    MMMPreferencePageVC *preferencePageVC = [storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([MMMPreferencePageVC class])];
    
    MMMDataSourceController *dsController = [MMMDataSourceController new];
    MMMPlayController *player = [MMMPlayController new];
    
    MMMPlayListController *playListController = [[MMMPlayListController alloc] initWithDataSource:dsController andPlayer:player];
    MMMSearchListController *searchListController = [[MMMSearchListController alloc] initWithDataSource:dsController andPlayer:player];
    MMMPreferencesController *preferencesController = [[MMMPreferencesController alloc] initWithDataSource:dsController andPlayer:player];

    [preferencesController loadPreferences];
    [playListController setPreferences:preferencesController];
    [searchListController setPreferences:preferencesController];

    [playPageVC setTrackController:playListController];
    [searchPageVC setTrackController:searchListController];
    [preferencePageVC setTrackController:preferencesController];
    
    _tabBarController = [MMMBaseTabBarController new];
    [_tabBarController setViewControllers:@[playPageVC, searchPageVC, preferencePageVC]];
    [_tabBarController setDelegate:self];
    
    [[_tabBarController tabBar] setBackgroundColor:[UIColor grayColor]];
    [[_tabBarController tabBar] setTintColor:[UIColor blackColor]];
    
    [window setRootViewController:_tabBarController];
}

#pragma mark - lifting keyboard

- (void)keyboardWillToggle:(NSNotification *)aNotification
{
    CGRect frame = [[[self tabBarController] tabBar] frame];
    CGRect keyboard = [[aNotification.userInfo valueForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    frame.origin.y = keyboard.origin.y - frame.size.height;
    
    [UIView animateWithDuration:[[aNotification.userInfo valueForKey:@"UIKeyboardAnimationDurationUserInfoKey"] floatValue] animations:^
    {
        [[[self tabBarController] tabBar] setFrame:frame];
    }];
}

#pragma mark - animating tab switching

- (nullable id<UIViewControllerAnimatedTransitioning>)tabBarController:(UITabBarController *)tabBarController
                     animationControllerForTransitionFromViewController:(UIViewController *)fromVC
                                                       toViewController:(UIViewController *)toVC
{
    return self;
}

- (NSTimeInterval)transitionDuration:(nullable id<UIViewControllerContextTransitioning>)transitionContext
{
    return .3;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    [[transitionContext containerView] addSubview:[toViewController view]];
    
    NSUInteger controllerIndex = [[_tabBarController viewControllers] indexOfObject:fromViewController];
    
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionPush];
    [animation setSubtype:(controllerIndex > _tabBarController.selectedIndex ? kCATransitionFromLeft : kCATransitionFromRight)];
    [animation setDuration:[self transitionDuration:transitionContext]];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
                                  kCAMediaTimingFunctionEaseOut]];
    [[[transitionContext containerView] layer] addAnimation:animation forKey:@"fadeTransition"];
    
    [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
}

@end
