//
//  MMMMainInterface.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/19/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMMBasisCreator : NSObject

- (void)setupBasicWindow:(UIWindow *)window;

@end
