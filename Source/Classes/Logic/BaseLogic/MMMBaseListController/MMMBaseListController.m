//
//  MMMBaseListController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/28/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseListController_Protected.h"

@implementation MMMBaseListController

- (instancetype)initWithDataSource:(MMMDataSourceController *)dsController andPlayer:(MMMPlayController *)player
{
    self = [super init];
    
    if (self != nil)
    {
        _dsController = (MMMDataSourceController *)dsController;
        _player = player;
        _isPlaying = NO;
        [self setDataSourceDelegate];
    }
    
    return self;
}

#pragma mark - MUST BE REDEFINE by CHILD !!!!!!!!!!!

- (void)setDataSourceDelegate
{
    abort();
}

#pragma mark - MMMBaseListController_Protected

- (void)playTrack:(MMMTrack *)track
{
    [self player:_player didStopPlayTrack:_playingTrack];
    
    if ([[track songFilePath] length] > 0 || [_preferences isAllowedToDownload])
    {
        _playingTrack = track;
        [self play];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Error"
                                    message:@"No WiFi enabled"
                                   delegate:nil
                          cancelButtonTitle:@"OK. I've gotit"
                          otherButtonTitles:nil] show];
        
        [self player:_player didStopPlayTrack:_playingTrack];
    }
}

#pragma mark - MMMPlayerDelegateProtocol

- (void)player:(MMMPlayController *)player didFinishedPlayTrack:(MMMTrack *)track
{
    NSInteger index = [_trackList indexOfObject:track];
    
//    [self player:player didUpdatePlayingProgress:.0 forTrack:track];
    
    if ([_trackList count] == 0)
    {
        [self player:player didStopPlayTrack:nil];
        
        return;
    }
    
    if (index == [_trackList count] - 1)
    {
        if (![_preferences mustContinueFromBegining])
        {
            [self player:player didStopPlayTrack:nil];
            
            return;
        }
        
        index = 0;
    }
    else
    {
        ++index;
    }

    [self playTrack:[_trackList objectAtIndex:index]];
}

- (void)player:(MMMPlayController *)player didStopPlayTrack:(MMMTrack *)track
{
    [player stop];
    _isPlaying = NO;
    [self player:player didUpdatePlayingProgress:.0 forTrack:track];
    [self player:player didChagedPlayingState:mNone forTrack:track];
//    [self isPlaying:NO track:track];
    [self setPlayingTrack:nil];
}

- (void)player:(MMMPlayController *)player didUpdatePlayingProgress:(CGFloat)time forTrack:(MMMTrack *)track
{
    if ([_trackList containsObject:track])
    {
        [_delegate updatePlayingProgress:time forTrack:track];
    }
}

- (void)player:(MMMPlayController *)player didChagedPlayingState:(PlayingCellState)playingState forTrack:(MMMTrack *)track;
{
    if ([_trackList containsObject:track])
    {
        [_delegate updateCellState:playingState forTrack:track];
    }
}

#pragma mark - MMMCollectionDSDelegateProtocol

- (void)willPlayTrack:(MMMTrack *)track
{
    if (_isPlaying && [_playingTrack isEqual:track])
    {
        [_player playPause];
    }
    else
    {
        [self playTrack:track];
    }
}

- (void)seekPlayPosition:(CGFloat)position forTrack:(MMMTrack *)track
{
    if (_isPlaying && [_playingTrack isEqual:track])
    {
        [_player seekPlayPosition:position];
    }
}

- (void)trackListDidChanged:(NSArray<MMMTrack *> *)trackList
{
    [self setTrackList:trackList];
    [_delegate trackListDidChanged:trackList];
}

#pragma mark - private

- (void)play
{
    [_player setDelegate:self];
    [_player setTrack:_playingTrack];
    _isPlaying = YES;
    [_player play];
}

@end
