//
//  MMMBaseListController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/28/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMCollectionDSDelegateProtocol.h"
#import "MMMPlayerDelegateProtocol.h"

@protocol MMMTrackListControllerDelegateProtocol;
@protocol MMMRequestPreferencesProtocol;

@class MMMDataSourceController;
@class MMMPlayController;
@class MMMTrack;

@interface MMMBaseListController : NSObject<MMMCollectionDSDelegateProtocol, MMMPlayerDelegateProtocol>

@property(nonatomic, weak) id<MMMTrackListControllerDelegateProtocol> delegate;

- (instancetype)initWithDataSource:(MMMDataSourceController *)dsController andPlayer:(MMMPlayController *)player;
- (void)setPreferences:(id<MMMRequestPreferencesProtocol>)controller;

@end
