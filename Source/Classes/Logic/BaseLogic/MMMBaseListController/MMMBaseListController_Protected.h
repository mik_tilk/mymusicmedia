//
//  MMMBaseListController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/28/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMRequestPreferencesProtocol.h"
#import "MMMTrackListControllerDelegateProtocol.h"
#import "MMMCollectionDSDelegateProtocol.h"
#import "MMMPlayerDelegateProtocol.h"
#import "MMMBaseListController.h"
#import "MMMDataSourceController.h"
#import "MMMTrack.h"
#import "MMMPlayController.h"
#import "MMMCellPalette.h"

@interface MMMBaseListController ()

@property(nonatomic, assign) id<MMMRequestPreferencesProtocol> preferences;
@property(nonatomic, strong) MMMDataSourceController *dsController;
@property(nonatomic, strong) MMMPlayController *player;
@property(nonatomic, strong) MMMTrack *playingTrack;
@property(nonatomic, copy) NSArray<MMMTrack *> *trackList;
@property(nonatomic, assign) BOOL isPlaying;

- (void)playTrack:(MMMTrack *)track;

@end
