//
//  MMMBaseCollectionDS.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/12/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionDS.h"
#import "MMMCollectionDSDelegateProtocol.h"
#import "MMMDisplayingCellModel.h"
#import "MMMBaseCell.h"
#import "MMMCellState.h"
#import "MMMTrack.h"

@interface MMMBaseCollectionDS ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property(nonatomic, copy) NSArray<MMMDisplayingCellModel *> *viewModelsList;
@property(nonatomic, copy) NSArray<MMMTrack *> *modelsList;

- (MMMTrack *)modelByViewModel:(MMMDisplayingCellModel *)displayingModel;
- (MMMDisplayingCellModel *)viewModelByModel:(MMMTrack *)model;
- (void)updateViewModelsList:(NSArray<MMMTrack *> *)trackList;

@end
