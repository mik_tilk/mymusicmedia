//
//  MMMBaseCollectionDS.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/12/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionDS_Protected.h"


@implementation MMMBaseCollectionDS

#pragma mark - MMMTrackListControllerDelegateProtocol

- (void)updatePlayingProgress:(CGFloat)time forTrack:(MMMTrack *)model
{
    MMMDisplayingCellModel *viewModel = [self viewModelByModel:model];
    
    NSInteger index = [_viewModelsList indexOfObject:viewModel];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    MMMBaseCell *cell = (MMMBaseCell *)[_collectionView cellForItemAtIndexPath:indexPath];
    
    [viewModel updatePlayingProgress:time];
    [cell updatePlayingProgressWithDisplayingModel:viewModel];
}

- (void)updateCellState:(PlayingCellState)cellState forTrack:(MMMTrack *)model
{
    MMMDisplayingCellModel *viewModel = [self viewModelByModel:model];
    
    NSInteger index = [_viewModelsList indexOfObject:viewModel];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    MMMBaseCell *cell = (MMMBaseCell *)[_collectionView cellForItemAtIndexPath:indexPath];
    
    [viewModel updateViewModelState:cellState];
    [cell updateCellByDisplayingModel:viewModel];
}

- (void)trackListDidChanged:(NSArray *)trackList
{
    [self updateViewModelsList:trackList];
    
    [[self collectionView] reloadData];
}

- (void)updateViewModelsList:(NSArray<MMMTrack *> *)trackList
{
    [self setModelsList:trackList];
    
    NSMutableArray *mutableList = [NSMutableArray array];
    
    for (MMMTrack *track in trackList)
    {
        MMMDisplayingCellModel *displayingModel = [[MMMDisplayingCellModel alloc] initModelWithTrack:track];
        [mutableList addObject:displayingModel];
    }

    [self setViewModelsList:(NSArray *)mutableList];
}

- (MMMTrack *)modelByViewModel:(MMMDisplayingCellModel *)displayingModel
{
    for (MMMTrack *model in _modelsList)
    {
        if ([displayingModel localId] == [model localId])
        {
            return model;
        }
    }
    
    return nil;
}

- (MMMDisplayingCellModel *)viewModelByModel:(MMMTrack *)model
{
    for (MMMDisplayingCellModel *viewModel in _viewModelsList)
    {
        if ([viewModel localId] == [model localId])
        {
            return viewModel;
        }
    }
    
    return nil;
}

//- (MMMTrack *)getTrackByLocalId:(NSString *)localId
//{
//    NSPredicate *filter = [NSPredicate predicateWithBlock:^BOOL (MMMTrack *track, NSDictionary *bindings)
//                           {
//                               return [track localId] == localId;  // Return YES for each object you want in filteredArray.
//                           }];
//    NSArray *filteredArray = [_trackList filteredArrayUsingPredicate:filter];
//    
//    return [filteredArray firstObject];
//}
//  - (void)animateCell:(UICollectionViewCell *)cell
//  {
//    CGRect finalCellFrame = cell.frame;
//
//    CGPoint translation = [[(UICollectionView *)[cell superview] panGestureRecognizer] translationInView:[cell superview]];
//
//    if (translation.y > 0)
//    {
//        cell.frame = CGRectMake(30, 0, 30, finalCellFrame.size.height);
//    }
//    else
//    {
//        cell.frame = CGRectMake(30, 0, 30, finalCellFrame.size.height);
//    }
//
//    [UIView animateWithDuration:0.4f animations:^(void)
//    {
//        cell.frame = finalCellFrame;
//    }];
//  }
#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_viewModelsList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

@end
