//
//  MMMBaseCollectionDS.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/12/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMTrackListControllerDelegateProtocol.h"

@protocol MMMCollectionDSDelegateProtocol;

@class MMMTrack;

@interface MMMBaseCollectionDS : NSObject<MMMTrackListControllerDelegateProtocol>

@property(nonatomic, weak) id<MMMCollectionDSDelegateProtocol> delegate;

@end
