//
//  MMMPlayListController.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/5/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMBaseListController.h"

@class MMMPlayTrackCell;
@class MMMTrack;

@interface MMMPlayListController : MMMBaseListController

@property(nonatomic, copy) void (^didCellLongPressHandler)(MMMPlayTrackCell *cell, MMMTrack *track);

- (void)startWithSavedTrackList;

@end
