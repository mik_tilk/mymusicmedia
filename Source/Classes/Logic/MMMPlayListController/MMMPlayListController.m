//
//  MMMPlayListController.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/5/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseListController_Protected.h"
#import "MMMPlayListController.h"

@implementation MMMPlayListController

#pragma mark - MUST BE REDEFINED

- (void)setDataSourceDelegate
{
    [[self dsController] setDelegatePlayListUpdate:self];
}

#pragma mark - .h

- (void)startWithSavedTrackList
{
    [self setTrackList:[[self dsController] loadSavedTracklist]];
    [self trackListDidChanged:[self trackList]];
}

- (void)playTrack:(MMMTrack *)track
{
    [super playTrack:track];
    
    if ([self playingTrack] != nil && [[self playingTrack] songFilePath] == nil)
    {
        [[self dsController] downloadSongForTrack:[self playingTrack]];
    }
}

#pragma mark - MMMCollectionDSDelegateProtocol

- (void)deleteTrack:(MMMTrack *)track;
{
    if ([track isEqual:[self playingTrack]])
    {
        [self player:[self player] didStopPlayTrack:[self playingTrack]];
    }
    
    [[self dsController] deleteTrack:track];
}

- (void)saveTrackList:(NSArray<MMMTrack *> *)trackList
{
    [self setTrackList:trackList];
    [[self dsController] savePlayList:trackList];
}

@end
