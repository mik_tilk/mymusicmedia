//
//  MMMDownloadResources.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/23/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMMDownloadDelegateProtocol;

@class MMMTrack;

@interface MMMDownloadResourcesManager : NSObject

@property(nonatomic, weak) id<MMMDownloadDelegateProtocol> delegate;

- (void)loadListResourcesInBackground:(NSArray<MMMTrack *> *)track;
- (void)downloadSongByURL:(NSString *)url forTrack:(MMMTrack *)track;

@end
