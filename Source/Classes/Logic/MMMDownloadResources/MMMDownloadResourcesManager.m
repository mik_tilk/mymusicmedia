//
//  MMMDownloadResources.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/23/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "AFHTTPRequestOperationManager.h"
#import "MMMDownloadDelegateProtocol.h"
#import "MMMDownloadResourcesManager.h"
#import "MMMFileSystemResolver.h"
#import "MMMCellPalette.h"
#import "MMMTrack.h"

@interface MMMDownloadResourcesManager ()<AVAudioRecorderDelegate>

@property(nonatomic, strong) NSOperationQueue *downloadQueue;
@property(nonatomic, strong) MMMFileSystemResolver *fileResolver;

@end

@implementation MMMDownloadResourcesManager

- (instancetype)init
{
    self = [super init];
    
    if (self != nil)
    {
        _downloadQueue = [NSOperationQueue new];
        [_downloadQueue setMaxConcurrentOperationCount:5];
        _fileResolver = [MMMFileSystemResolver new];
    }
    
    return self;
}

- (void)loadListResourcesInBackground:(NSArray<MMMTrack *> *)trackList
{
    if ([_downloadQueue operationCount] > 0)
    {
        [self cancelAllPreviousDownloads];
    }
    
    NSBlockOperation *downloadResources = [NSBlockOperation new];
    __weak NSBlockOperation *weakDownloadResources = downloadResources;

    [weakDownloadResources addExecutionBlock:^
    {
        for (MMMTrack *track in trackList)
        {
            if ([track artworkImageURL])
            {
                NSBlockOperation *downloadImage = [NSBlockOperation new];
                __weak NSBlockOperation *weakDownloadImage = downloadImage;
                
                [weakDownloadImage addExecutionBlock:^
                {
                    UIImage *image = [UIImage imageWithData:[self loadResource:[track artworkImageURL]]];
                    NSString *cachedFileName = [_fileResolver saveToCacheImage:image
                                                                  withFileName:[[track artworkImageURL] lastPathComponent]];
                    
                    image = [UIImage imageWithContentsOfFile:[_fileResolver getFullFilePath:cachedFileName]];
                    [track setArtworkImage:image];
                    [track setArtworkImageFilePath:cachedFileName];
                   
                    if ([track artworkImage])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^
                        {
                            [_delegate searchResultsListDidChangedTrack:track];
                        });
                    }
                }];
                
                [_downloadQueue addOperation:downloadImage];
            }

            if ([track waveformImageURL])
            {
                NSBlockOperation *downloadWaveform = [NSBlockOperation new];
                __weak NSBlockOperation *weakDownloadWaveform = downloadWaveform;
                
                [weakDownloadWaveform addExecutionBlock:^
                {
                    UIImage *image = [UIImage imageWithData:[self loadResource:[track waveformImageURL]]];
                    image = [self colorizeImage:image withColor:MMMCOLOR_1];
                    NSString *cachedFileName = [_fileResolver saveToCacheImage:image
                                                                  withFileName:[[track waveformImageURL] lastPathComponent]];
                    
                    image = [UIImage imageWithContentsOfFile:[_fileResolver getFullFilePath:cachedFileName]];
                    [track setWaveformImage:image];
                    [track setWaveformImageFilePath:cachedFileName];

                    if ([track waveformImage])
                    {
                        dispatch_async(dispatch_get_main_queue(), ^
                        {
                            [_delegate searchResultsListDidChangedTrack:track];
                        });
                    }
                }];
                
                [_downloadQueue addOperation:downloadWaveform];
            }
        }
    }];
    
    [_downloadQueue addOperation:downloadResources];
}

- (void)downloadSongByURL:(NSString *)stringURL forTrack:(MMMTrack *)track
{
    NSOperationQueue *queue = [NSOperationQueue new];
    
    NSBlockOperation *downloadSong = [NSBlockOperation new];
    __weak NSBlockOperation *weakDownloadSong = downloadSong;
    
    [weakDownloadSong addExecutionBlock:^
    {
        NSError *error;
        
        NSData *data = [self loadResource:stringURL];
        
        if (error)
        {
            MLOG(@"downloadError - %@", [error localizedDescription]);
        }
        else
        {
            MLOG(@"download size - %lu", (unsigned long)[data length]);
            [_delegate didDownloadSongData:data forTrack:track];
        }
    }];
    
    [queue addOperation:downloadSong];
}

#pragma mark - private

- (void)cancelAllPreviousDownloads
{
    [_downloadQueue cancelAllOperations];
}

- (NSData *)loadResource:(NSString *)stringURL
{
    NSError *downloadError = nil;
    
    NSURL *resourceURL = [NSURL URLWithString:stringURL];
    NSData *resourceData = [NSData dataWithContentsOfURL:resourceURL
                                                 options:kNilOptions
                                                   error:&downloadError];
    
    if (downloadError)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            MLOG(@"downloadError - %@", [downloadError localizedDescription]);
        });
        
        return nil;
    }
    else
    {
        return resourceData;
    }
}

- (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color
{
    if (image == nil)
    {
        return image;
    }
    
    CGFloat rate = .3;
    CGRect rect = CGRectMake(0, 0, image.size.width * rate, image.size.height * rate);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1);
    [color setFill];
    UIRectFill(rect);
    [image drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1];
    
    UIImage *colorizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return colorizedImage;
}

@end
