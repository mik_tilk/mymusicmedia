//
//  MMMPreferenceCollectionDS.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPreferencesListDelegateProtocol.h"
#import "MMMPreferencesDSDelegateProtocol.h"
#import "MMMPreferenceCollectionDS.h"
#import "MMMPropertyCellModel.h"
#import "MMMPropertyCell.h"
#import "MMMProperties.h"

@interface MMMPreferenceCollectionDS ()<UICollectionViewDelegate, UICollectionViewDataSource>

@property(weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property(nonatomic, copy) NSArray<MMMPropertyCellModel *> *displayingModelsList;

@end

@implementation MMMPreferenceCollectionDS

- (void)updateViewModelsList:(MMMProperties *)properties
{
    NSMutableArray *mutableList = [NSMutableArray array];
    
    for (NSString *property in [properties propertiesList])
    {
        id value = [[properties propertiesList] objectForKey:property];
        MMMPropertyCellModel *displayingModel = [[MMMPropertyCellModel alloc] initModelWithProperty:property andValue:value];
        [mutableList addObject:displayingModel];
    }
    
    _displayingModelsList = (NSArray *)mutableList;
}

#pragma mark - MMMPreferencesListDelegateProtocol

- (void)propertiesDidChanged:(MMMProperties *)properties
{
    [self updateViewModelsList:properties];
    
    [_collectionView reloadData];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *property = [[_displayingModelsList objectAtIndex:[indexPath item]] name];
    [_delegate didChangeProperty:property];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_displayingModelsList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^
    {
        identifier = NSStringFromClass([MMMPropertyCell class]);
    });
    
    MMMPropertyCell *cell = (MMMPropertyCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                                                                         forIndexPath:indexPath];
    [cell setupWithDisplayingModel:[[self displayingModelsList] objectAtIndex:[indexPath item]]];
    
    return cell;
}

@end
