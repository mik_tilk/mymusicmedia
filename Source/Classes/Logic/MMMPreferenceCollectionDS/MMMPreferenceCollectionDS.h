//
//  MMMPreferenceCollectionDS.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPreferencesListDelegateProtocol.h"

@protocol MMMPreferencesDSDelegateProtocol;

@interface MMMPreferenceCollectionDS : NSObject<MMMPreferencesListDelegateProtocol>

@property(nonatomic, weak) id<MMMPreferencesDSDelegateProtocol> delegate;

@end
