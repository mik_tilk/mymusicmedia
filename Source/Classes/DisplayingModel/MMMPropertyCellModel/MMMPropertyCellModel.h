//
//  MMMPropertyCellModel.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMMPropertyCellModel : NSObject

@property(strong, nonatomic, readonly) NSString *name;
@property(assign, nonatomic, readonly) BOOL value;

- (instancetype)initModelWithProperty:(NSString *)property andValue:(id)value;
- (void)updateModelWithProperty:(NSString *)property andValue:(id)value;

@end
