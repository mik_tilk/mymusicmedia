//
//  MMMPropertyCellModel.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPropertyCellModel.h"

@implementation MMMPropertyCellModel

- (instancetype)initModelWithProperty:(NSString *)property andValue:(id)value
{
    self = [super init];
    
    if (self != nil)
    {
        _name = property;
        _value = [value boolValue];
    }
    
    return self;
}

- (void)updateModelWithProperty:(NSString *)property andValue:(id)value
{
    _name = property;
    _value = [value boolValue];
}

@end
