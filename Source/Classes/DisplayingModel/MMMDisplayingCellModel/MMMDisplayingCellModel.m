//
//  MMTrackCellModel.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMDisplayingCellModel.h"
#import "MMMTrack.h"

@implementation MMMDisplayingCellModel

static NSString *noImage = @"spirale.png";

- (instancetype)initModelWithTrack:(MMMTrack *)track
{
    self = [super init];
    
    if (self != nil)
    {
        _title = [track songTitle];
        _artist = [track artist];
        _localId = [track localId];
        _durationInt = [track duration] / 1000;
        _duration = [self durationFormatter:_durationInt];
        
        if ([track artworkImage] != nil)
        {
            _albumIcon = [track artworkImage];
        }
        else
        {
            _albumIcon = [UIImage imageNamed:noImage];
        }
        
        _waveformImage = [track waveformImage];
        _playingProgress = 0;
        _playingProgressStr = [self durationFormatter:_playingProgress];
        _cellState = mNone;
//        _isPrepearingPlay = NO;
//        _isPlaying = NO;
    }
    
    return self;
}

- (void)updatePlayingProgress:(CGFloat)progress
{
    _playingProgress = progress;
    _playingProgressStr = [self durationFormatter:_playingProgress];
}

- (NSString *)durationFormatter:(NSInteger)number
{
    NSInteger seconds = 0;
    NSInteger minutes = 0;
    NSInteger hours = 0;

    if (number != 0)
    {
        seconds = number % 60;
        minutes = (number / 60) % 60;
        hours = number / 3600;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (void)updateViewModelImages:(MMMTrack *)track
{
    if ([track artworkImage] != nil)
    {
        _albumIcon = [track artworkImage];
    }
    
    _waveformImage = [track waveformImage];
}

- (void)updateViewModelState:(PlayingCellState)cellState
{
    _cellState = cellState;
}

@end
