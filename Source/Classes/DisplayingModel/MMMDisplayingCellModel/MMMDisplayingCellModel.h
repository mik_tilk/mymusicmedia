//
//  MMMDisplayingCellModel.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMMCellState.h"

@class MMMTrack;

@interface MMMDisplayingCellModel : NSObject

@property(strong, nonatomic, readonly) NSString *title;
@property(strong, nonatomic, readonly) NSString *artist;
@property(strong, nonatomic, readonly) NSString *localId;
@property(strong, nonatomic, readonly) NSString *duration;
@property(assign, nonatomic, readonly) NSInteger durationInt;
@property(strong, nonatomic, readonly) UIImage *albumIcon;
@property(strong, nonatomic, readonly) UIImage *waveformImage;
@property(assign, nonatomic, readonly) CGFloat playingProgress;
@property(strong, nonatomic, readonly) NSString *playingProgressStr;
@property(assign, nonatomic, readonly) PlayingCellState cellState;
@property(assign, nonatomic, readonly) BOOL isPrepearingPlay;
@property(assign, nonatomic, readonly) BOOL isPlaying;

- (instancetype)initModelWithTrack:(MMMTrack *)track;
- (void)updatePlayingProgress:(CGFloat)progress;
- (void)updateViewModelImages:(MMMTrack *)track;
- (void)updateViewModelState:(PlayingCellState)cellState;

@end
