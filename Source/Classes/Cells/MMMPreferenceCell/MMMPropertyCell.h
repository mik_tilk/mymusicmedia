//
//  MMMPreferenceCell.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMMPropertyCellModel;

@interface MMMPropertyCell : UICollectionViewCell

- (void)setupWithDisplayingModel:(MMMPropertyCellModel *)displayingModel;

@end
