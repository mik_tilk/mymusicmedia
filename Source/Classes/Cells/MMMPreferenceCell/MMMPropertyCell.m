//
//  MMMPreferenceCell.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPropertyCell.h"
#import "MMMPropertyCellModel.h"
#import "MMMCellPalette.h"

@interface MMMPropertyCell ()

@property(weak, nonatomic) IBOutlet UILabel *nameLabel;
@property(weak, nonatomic) IBOutlet UISwitch *aSwitch;

@end

@implementation MMMPropertyCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:MMMCOLOR_1];
}

- (void)setupWithDisplayingModel:(MMMPropertyCellModel *)displayingModel
{
    [_nameLabel setText:[displayingModel name]];
    [_aSwitch setOn:[displayingModel value]];
}

@end
