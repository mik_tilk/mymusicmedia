//
//  MMTrackCell.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCell_Protected.h"
#import "MMMSearchTrackCell.h"

@interface MMMSearchTrackCell ()

@property(weak, nonatomic) IBOutlet UIImageView *addButton;

@end

@implementation MMMSearchTrackCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UITapGestureRecognizer *tapAddButton = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAddButton:)];
    [tapAddButton setNumberOfTapsRequired:1];
    [_addButton addGestureRecognizer:tapAddButton];
}

- (void)tapAddButton:(id)sender
{
    if (_didAddButtonTapHandler != nil)
    {
        _didAddButtonTapHandler(self);
    }
}

@end
