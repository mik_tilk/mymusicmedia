//
//  MMMBaseCell.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCell_Protected.h"

@implementation MMMBaseCell

static const CGFloat cornerRadius = 4;
static const CGFloat separatorHeight = 3;
static const CGFloat gradientWidth = 10;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self setBackgroundColor:MMMCOLOR_1];
    [[self layer] setCornerRadius:gradientWidth];
    
//    [self addLinearGradientToBackgroundWithColor:MMMCOLOR_1 transparentToOpaque:NO];

//    CALayer *_selectShadeLayer = [CALayer new];
//    
//    CGRect rect = [self frame];
//    rect.size.height = rect.size.height - gradientWidth - 5;
//    rect.size.width = rect.size.width - gradientWidth - 3;
//    rect.origin.x = rect.origin.x + (gradientWidth + 3) / 2;
//    rect.origin.y = rect.origin.y + (gradientWidth + 5) / 2;
//    [_selectShadeLayer setFrame:rect];
//
//    [_selectShadeLayer setFrame:rect];
//    [_selectShadeLayer setBackgroundColor:[MMMCOLOR_1 CGColor]];
//    [_selectShadeLayer setCornerRadius:gradientWidth];
////    [_selectShadeLayer setBorderWidth:20];
////    [_selectShadeLayer setShadowOpacity:0.99f];
//    [_selectShadeLayer setShadowRadius:gradientWidth + 5];
//    [_selectShadeLayer setShadowColor:[[UIColor whiteColor] CGColor]];
//    [_selectShadeLayer setShadowOffset:CGSizeZero];
    
//    [[[self contentView] layer] insertSublayer:_selectShadeLayer atIndex:0];
    
//    CALayer *separatorLayer = [CALayer layer];
//    
//    CGRect rect = [self frame];
//    rect.origin.y = rect.size.height;
//    rect.size.height = separatorHeight;
//
//    [separatorLayer setFrame:rect];
//
//    [separatorLayer setBackgroundColor:[[UIColor lightGrayColor] CGColor]];
//    [separatorLayer setCornerRadius:cornerRadius];
//    [separatorLayer setShadowColor:[UIColor whiteColor].CGColor];
//    [separatorLayer setShadowOpacity:1];
//    [separatorLayer setShadowOffset:CGSizeZero];
////    [separatorLayer setMasksToBounds:NO];
//    [separatorLayer setShadowRadius:cornerRadius];
//
//    [[[self contentView] layer] insertSublayer:separatorLayer atIndex:1];
    
    CALayer *backgroundLayer = [CALayer new];
    [backgroundLayer setFrame:[_waveformView frame]];
    [backgroundLayer setBackgroundColor:[MMMCOLOR_5 CGColor]];
    
    [[[self contentView] layer] insertSublayer:backgroundLayer below:[_waveformView layer]];
    
    _shadeLayer = [CALayer new];
    CGRect shadeRect = [_albumIcon bounds];
    
    [_shadeLayer setFrame:shadeRect];
    [_shadeLayer setBackgroundColor:[[UIColor lightGrayColor] CGColor]];
    [_shadeLayer setOpacity:0.5];
    
    _maskLayer = [CALayer new];
    [_maskLayer setFrame:[_waveformView frame]];

    [_maskLayer setBackgroundColor:[MMMCOLOR_3 CGColor]];
    NSMutableDictionary *newActions = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                       [NSNull null], @"onOrderIn",
                                       [NSNull null], @"onOrderOut",
                                       [NSNull null], @"sublayers",
                                       [NSNull null], @"contents",
                                       [NSNull null], @"bounds",
                                       [NSNull null], @"position",
                                       [NSNull null], @"hidden",
                                       nil];
    [_maskLayer setActions:newActions];

    [[[self contentView] layer] insertSublayer:_maskLayer below:[_waveformView layer]];
    
    [[self activityIndicator] setHidesWhenStopped:YES];
    [[self activityIndicator] setColor:[UIColor whiteColor]];
    
    UITapGestureRecognizer *tapOnCellRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCell)];
    [[self contentView] addGestureRecognizer:tapOnCellRecognizer];

    UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnWaveform:)];
    [panRecognizer setCancelsTouchesInView:NO];
    [_waveformView addGestureRecognizer:panRecognizer];
    [self setIsPanStarted:NO];
    
    [[[self playingIndicator] layer] setZPosition:200];
}

- (void)setSelected:(BOOL)selected
{
    if (selected)
    {
        [[self layer] setShadowRadius:gradientWidth];
        [[self layer] setShadowOpacity:.8];
        [[self layer] setShadowColor:[[UIColor whiteColor] CGColor]];
        [[self layer] setShadowOffset:CGSizeZero];
//        [_waveformView setImage:[self colorizeImage:[_waveformView image] withColor:MMMCOLOR_4]];
//        self.backgroundColor = MMMCOLOR_4;
    }
    else
    {
        [[self layer] setShadowOpacity:.0];
//        [_waveformView setImage:[self colorizeImage:[_waveformView image] withColor:MMMCOLOR_1]];
//        self.backgroundColor = MMMCOLOR_1;
    }
    
    [super setSelected:selected];
}

#pragma mark - MMMBaseCell.h

- (void)setupWithDisplayingModel:(MMMDisplayingCellModel *)displayingModel
{
    [_title setText:[displayingModel title]];
    [_artist setText:[displayingModel artist]];
    [_duration setText:[displayingModel duration]];
    [self updateCellByDisplayingModel:displayingModel];
    [self updatePlayingProgressWithDisplayingModel:displayingModel];
}

- (void)updateCellByDisplayingModel:(MMMDisplayingCellModel *)displayingModel
{
    [_albumIcon setImage:[displayingModel albumIcon]];
    [_waveformView setImage:[displayingModel waveformImage]];
    [self updateCellState:[displayingModel cellState]];
}

- (void)updatePlayingProgressWithDisplayingModel:(MMMDisplayingCellModel *)displayingModel
{
    [_playingDuration setText:[displayingModel playingProgressStr]];
    CGFloat rate = 0;
    
    if ([displayingModel durationInt] != 0)
    {
        rate = CGRectGetWidth([_waveformView frame]) / [displayingModel durationInt];
    }

    CGRect maskRect = [_waveformView frame];
    CGFloat width = [displayingModel playingProgress] * rate;
    
    if (width > _waveformView.bounds.size.width)
    {
        width = _waveformView.bounds.size.width;
    }
    
    maskRect.size.width = width;
    
    [_maskLayer setFrame:maskRect];
}

- (NSString *)getTitle
{
    return [[self title] text];
}

#pragma mark - gesture selectors

- (void)tapOnCell
{
    if (_didCellTapHandler != nil)
    {
        _didCellTapHandler(self);
    }
}

- (void)panOnWaveform:(UIPanGestureRecognizer *)pan
{
    CGPoint point = [pan translationInView:_waveformView];
    
    if ([pan state] == UIGestureRecognizerStateBegan)
    {
        [self setDx:_maskLayer.bounds.size.width];
        [self setIsPanStarted:YES];
    }
    
    if ([self isPanStarted] && [pan state] == UIGestureRecognizerStateChanged)
    {
        CGFloat rate = [self durationInt] / _waveformView.frame.size.width;
        
        CGFloat position = (_dx + point.x) * rate;
        
        if (_didWaveformPanHandler != nil)
        {
            _didWaveformPanHandler(position, self);
        }
    }
}

#pragma mark - private

- (void)updateCellState:(PlayingCellState)cellState
{
    switch (cellState)
    {
        case mNone:
            [[self activityIndicator] stopAnimating];
            [self unShadeView:_albumIcon];
            
            [_playingIndicator setHidden:YES];
            [_playingDuration setHidden:YES];
            [self setSelected:NO];
            
            [[[self playingIndicator] layer] removeAnimationForKey:@"rotation"];
            break;
            
        case mPrepearingStreaming:
            [[self activityIndicator] startAnimating];
            [self shadeView:_albumIcon];
            [self setSelected:YES];
            break;
            
        case mPlaying:
            [[self activityIndicator] stopAnimating];
            [self unShadeView:_albumIcon];
            
            [_playingIndicator setHidden:NO];
            [_playingDuration setHidden:NO];
            [self setSelected:YES];
            
            CABasicAnimation *animation = [CABasicAnimation
                                           animationWithKeyPath:@"transform.rotation.y"];
            animation.fromValue = @(0);
            animation.toValue = @(2 * M_PI);
            animation.repeatCount = INFINITY;
            animation.duration = 5.0;
            
            [[[self playingIndicator] layer] addAnimation:animation forKey:@"rotation"];
            
            CATransform3D transform = CATransform3DIdentity;
            transform.m34 = 1.0 / 500.0;
            [[[self playingIndicator] layer] setTransform:transform];
            
            break;
    }
}

- (NSInteger)durationInt
{
    NSArray *subStrings = [[_duration text] componentsSeparatedByString:@":"];
    NSInteger hours = [[subStrings objectAtIndex:0] integerValue] * 3600;
    NSInteger mins = [[subStrings objectAtIndex:1] integerValue] * 60;
    NSInteger sec = [[subStrings objectAtIndex:2] integerValue];
    
    return (hours + mins + sec);
}

- (void)shadeView:(UIView *)view
{
    if (![[[view layer] sublayers] containsObject:_shadeLayer])
    {
        [[view layer] addSublayer:_shadeLayer];
    }
}

- (void)unShadeView:(UIView *)view
{
    if ([[[view layer] sublayers] containsObject:_shadeLayer])
    {
        [_shadeLayer removeFromSuperlayer];
    }
}

- (UIImage *)colorizeImage:(UIImage *)image withColor:(UIColor *)color
{
    if (image == nil)
    {
        return image;
    }
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1);
    [color setFill];
    UIRectFill(rect);
    [image drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1];
    
    UIImage *colorizedImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return colorizedImage;
}

//- (void)addLinearGradientToBackgroundWithColor:(UIColor *)theColor transparentToOpaque:(BOOL)transparentToOpaque
//{
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    
//    //the gradient layer must be positioned at the origin of the view
//    CGRect gradientFrame = [self frame];
//    gradientFrame.origin.x = 0;
//    gradientFrame.origin.y = 0;
//    gradient.frame = gradientFrame;
//    
//    //build the colors array for the gradient
//    NSArray *colors = [NSArray arrayWithObjects:
//                       (id)[theColor CGColor],
//                       (id)[[theColor colorWithAlphaComponent:0.9f] CGColor],
//                       (id)[[theColor colorWithAlphaComponent:0.6f] CGColor],
//                       (id)[[theColor colorWithAlphaComponent:0.4f] CGColor],
//                       (id)[[theColor colorWithAlphaComponent:0.3f] CGColor],
//                       (id)[[theColor colorWithAlphaComponent:0.1f] CGColor],
//                       (id)[[UIColor clearColor] CGColor],
//                       nil];
//    
//        //reverse the color array if needed
//    if(transparentToOpaque)
//    {
//        colors = [[colors reverseObjectEnumerator] allObjects];
//    }
//    
//        //apply the colors and the gradient to the view
//    gradient.colors = colors;
//    
//    [[self layer] insertSublayer:gradient atIndex:0];
//}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
        //    UIScrollViewDelayedTouchesBeganGestureRecognizer
//    
//    if ([otherGestureRecognizer state] == (UIGestureRecognizerStateBegan | UIGestureRecognizerStateChanged))
//    {
//        return NO;
//    }
//    else
//    {
//        return YES;
//    }
    return YES;
}

@end