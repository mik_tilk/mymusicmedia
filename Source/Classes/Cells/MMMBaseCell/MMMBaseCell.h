//
//  MMMTrackCell.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMMDisplayingCellModel;
@protocol MMMCellGestureDelegateProtocol;

@interface MMMBaseCell : UICollectionViewCell

@property(nonatomic, copy) void (^didCellTapHandler)(MMMBaseCell *cell);
@property(nonatomic, copy) void (^didWaveformPanHandler)(CGFloat position, MMMBaseCell *cell);

- (void)setupWithDisplayingModel:(MMMDisplayingCellModel *)model;
- (void)updatePlayingProgressWithDisplayingModel:(MMMDisplayingCellModel *)displayingModel;
- (void)updateCellByDisplayingModel:(MMMDisplayingCellModel *)displayingModel;

- (NSString *)getTitle;

@end
