//
//  MMMBaseCell.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/7/15.
//  Copyright © 2015 NIX. All rights reserved.
//
#import "MMMDisplayingCellModel.h"
#import "MMMCellPalette.h"
#import "MMMCellState.h"
#import "MMMBaseCell.h"

@interface MMMBaseCell ()

@property(weak, nonatomic) IBOutlet UILabel *title;
@property(weak, nonatomic) IBOutlet UILabel *artist;
@property(weak, nonatomic) IBOutlet UIImageView *albumIcon;
@property(weak, nonatomic) IBOutlet UIImageView *waveformView;
@property(weak, nonatomic) IBOutlet UIImageView *playingIndicator;
@property(weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property(weak, nonatomic) IBOutlet UILabel *duration;
@property(weak, nonatomic) IBOutlet UILabel *playingDuration;

@property(nonatomic, assign) BOOL isPanStarted;
@property(nonatomic, assign) CGFloat dx;
@property(nonatomic, strong) CALayer *maskLayer;
@property(nonatomic, strong) CALayer *shadeLayer;
@property(nonatomic, strong) CALayer *selectShadeLayer;

@end
