//
//  MMMTrackListCollectionVC.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionVC.h"

@class MMMPlayListController;

@interface MMMPlayPageVC : MMMBaseCollectionVC

- (void)setTrackController:(MMMPlayListController *)controller;

@end
