//
//  MMMTrackListCollectionVC.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPlayPageVC.h"
#import "MMMBaseCell.h"
#import "MMMPlayListController.h"
#import "MMMPlayListCollectionDS.h"
#import "MMMDragCellController.h"

@interface MMMPlayPageVC ()

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, weak) IBOutlet MMMPlayListCollectionDS *collectionDS;
@property(weak, nonatomic) IBOutlet MMMDragCellController *dragCellController;

@property(nonatomic, strong) MMMPlayListController *playListController;

@end

@implementation MMMPlayPageVC

- (void)setTrackController:(MMMPlayListController *)controller
{
    _playListController = controller;
    [_playListController setDelegate:_collectionDS];
    [_collectionDS setDelegate:_playListController];
    [_dragCellController setupWithPlayListCollectionDS:_collectionDS];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:_dragCellController action:@selector(handlePanOnPage:)];
    [longPressGesture setDelegate:_dragCellController];
    [_collectionView addGestureRecognizer:longPressGesture];

    [_collectionView setBackgroundColor:MMMCOLOR_1];

    [_playListController startWithSavedTrackList];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [_collectionView reloadData];
//    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
//    flow.itemSize = CGSizeMake(360*iPhoneFactorX, 438*iPhoneFactorX);
//    flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    flow.minimumInteritemSpacing = 0;
//    flow.minimumLineSpacing = 0;
//    
//    
//    [mainCollectionView reloadData];
//    mainCollectionView.collectionViewLayout = flow;
}

@end
