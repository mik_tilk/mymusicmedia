//
//  MMMPreferencePageVC.h
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionVC.h"

@class MMMPreferencesController;

@interface MMMPreferencePageVC : MMMBaseCollectionVC

- (void)setTrackController:(MMMPreferencesController *)controller;

@end
