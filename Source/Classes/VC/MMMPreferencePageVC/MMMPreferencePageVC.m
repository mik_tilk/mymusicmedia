//
//  MMMPreferencePageVC.m
//  MyMusicMedia
//
//  Created by Kholomeev on 11/6/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMPreferencePageVC.h"
#import "MMMPreferencesController.h"
#import "MMMPreferenceCollectionDS.h"
#import "MMMCellPalette.h"

@interface MMMPreferencePageVC ()

@property(nonatomic, weak) IBOutlet UICollectionView *preferenceCollectionView;
@property(nonatomic, weak) IBOutlet MMMPreferenceCollectionDS *collectionDS;

@property(nonatomic, strong) MMMPreferencesController *preferencesController;

@end

@implementation MMMPreferencePageVC

- (void)setTrackController:(MMMPreferencesController *)controller
{
    _preferencesController = controller;
    [_preferencesController setDelegate:_collectionDS];
    [_collectionDS setDelegate:_preferencesController];
    [_preferencesController updateCollectionDS];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_preferenceCollectionView setBackgroundColor:MMMCOLOR_1];
}

@end
