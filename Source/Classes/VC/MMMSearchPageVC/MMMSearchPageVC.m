//
//  MMMBaseSearchDisplayControllerVC.m
//  MyMusicMedia
//
//  Created by Kholomeev on 10/9/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMSearchPageVC.h"
#import "MMMSearchListController.h"
#import "MMMSearchResultsCollectionDS.h"

@interface MMMSearchPageVC ()

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property(nonatomic, weak) IBOutlet MMMSearchResultsCollectionDS *collectionDS;

@property(nonatomic, strong) MMMSearchListController *searchListController;

@end

@implementation MMMSearchPageVC

- (void)setTrackController:(MMMSearchListController *)controller
{
    _searchListController = controller;
    [_searchListController setDelegate:_collectionDS];
    [_collectionDS setDelegate:_searchListController];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [_collectionView setBackgroundColor:MMMCOLOR_1];
    
//    [_collectionView setBounds:[[UIScreen mainScreen] bounds]];
    [_searchBar setDelegate:_searchListController];
    [_searchBar setShowsCancelButton:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    if ([[_collectionView indexPathsForVisibleItems] count] == 0)
    {
        [[self searchBar] becomeFirstResponder];
    }
    
    [_collectionView reloadData];
    
    [super viewDidAppear:animated];
}

@end