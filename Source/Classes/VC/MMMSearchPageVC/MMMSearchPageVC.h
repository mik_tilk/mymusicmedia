//
//  MMMBaseSearchDisplayControllerVC.h
//  MyMusicMedia
//
//  Created by Kholomeev on 10/9/15.
//  Copyright © 2015 NIX. All rights reserved.
//

#import "MMMBaseCollectionVC.h"

@class MMMSearchListController;

@interface MMMSearchPageVC : MMMBaseCollectionVC

- (void)setTrackController:(MMMSearchListController *)controller;

@end
